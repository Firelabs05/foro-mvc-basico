<?php
##### CONTROLADOR DEL PERFIL ########
if (!isset($_GET['mode'])) {
  $_GET['mode'] = "pagina_perfil";
}
// Se Añade la informacion a los DICCIONARIOS
#### DICCIONARIOS DE LA MIJA
$DICCIONARIO_MIJA +=[
  'Perfil' => [
    0 => 'perfil_user.png',
    1 => 'perfil/'
  ]
];
$DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
// Se empieza a crear la PAGINA añade la cabecera
$html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);
switch ($_GET['mode']) {
  case 'editar_perfil':
  if (isset($_SESSION['id_user'])) {
    // Se pone el nav adecuado al tipo de usuario
    $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
    if ($_POST) {
      include ('controllers/perfil/editar_perfil_controller.php');
    } else {
      // Se crea el cuerpo
      $DICCIONARIO_CONTENIDO['USER'] = $_SESSION['users'][$_SESSION['id_user']]['user'];
      $DICCIONARIO_CONTENIDO['FIRMA'] = $_SESSION['users'][$_SESSION['id_user']]['firma'];
      $DICCIONARIO_CONTENIDO['BIOGRAFIA'] = $_SESSION['users'][$_SESSION['id_user']]['biografia'];
      $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(EDITAR_PERFIL,$DICCIONARIO_CONTENIDO);
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
    }
  } else {
    #redirecciona a index
  }
  break;
  default:
  if(isset($_GET['id_user'])
  && array_key_exists($_GET['id_user'],$_SESSION['users'])){
    $tema = new tema_model();
    $tema->set_id_user_d($_GET['id_user']);
    $tema->get_tema_user();
    $data = $tema->get_data();

    // Se le pasa el contenido al DICCIONARIO
    $imagen = 'view/img/user_perfil/'.$_SESSION['users'][$_GET['id_user']]['img'];
    $user = $_SESSION['users'][$_GET['id_user']]['user'];
    if($__->__USER_ACT($_SESSION['users'][$_GET['id_user']]['ultima_con'])){
      $imagen_online = 'view/img/online.png';
    }else {
      $imagen_online = 'view/img/offline.png';
    }
    $tipo = $__->__USER_TIP($_SESSION['users'][$_GET['id_user']]['permiso']);
    $rango = $__->__USER_RANG($_SESSION['users'][$_GET['id_user']]['rango']);
    $cantidad_tema = $data[0]['temas'];
    $edad = $_SESSION['users'][$_GET['id_user']]['edad'];
    $fecha = $_SESSION['users'][$_GET['id_user']]['fecha_reg'];
    $biografia = $_SESSION['users'][$_GET['id_user']]['biografia'];
    $firma = $__->BBcode($_SESSION['users'][$_GET['id_user']]['firma']);
    $cantidad_mensaje = $_SESSION['users'][$_GET['id_user']]['mensajes'];

    $DICCIONARIO_CONTENIDO += [
      'IMAGEN' => $imagen,
      'USER' => $user,
      'IMAGEN_ONLINE' => $imagen_online,
      'TIPO' => $tipo,
      'RANGO' => $rango,
      'CANTIDAD_TEMA' => $cantidad_tema,
      'CANTIDAD_MENSAJE' => $cantidad_mensaje,
      'EDAD' => $edad,
      'FECHA_REGISTRO' => $fecha,
      'BIOGRAFIA' => $biografia,
      'FIRMA' => $firma
    ];
    $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(PERFIL,$DICCIONARIO_CONTENIDO);
    if(isset($_SESSION['id_user'])){
      if($_GET['id_user'] === $_SESSION['id_user']){
        $boton = new display_user();
        $DICCIONARIO_NAV['BOTON'] = $boton->dinamic_boton();
      }
    }
    // Se pone el nav adecuado al tipo de usuario
    $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
    // Se añade el contenido del index
    $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
  }else {
    header('location: ?view=index');
  }
  break;
}
// Se añade el footer
$html .= $__TEMPLATE->display_footer(FOOTER);
// Se imprime
echo $html;

?>
