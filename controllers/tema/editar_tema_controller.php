<?php
if (isset($_POST['contenido']) && isset($_POST['tema'])
  && strlen($_POST['contenido']) >= CANTIDAD_CONTENIDO
  && strlen($_POST['tema']) >= CANTIDAD_TEMA ) {
    /// Se edita el nuevo tema a travez de su clase
    $tema->set_nombre($_POST['tema']);
    $tema->set_contenido($_POST['contenido']);

    if (isset($_POST['anuncio']) && $_POST['anuncio'] == true) {
      $tema->update(true); // edita el tema a la base de datos
    } else {
      $tema->update(); // edita el tema a la base de datos
    }
    $url->set_id_2($_GET['id_foro']);
    $url->set_titulo($_POST['tema']);
    $url->set_id_1($_GET['id_tema']);
    header('location: tema/'.$url->url_amigable());
}else {
  header('location: index.php?view=tema&pagina_todas&id_foro='.$_GET['id_foro']);
}
?>
