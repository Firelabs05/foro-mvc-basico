<?php
if (isset($_POST['contenido']) && isset($_POST['tema'])
  && strlen($_POST['contenido']) >= CANTIDAD_CONTENIDO
  && strlen($_POST['tema']) >= CANTIDAD_TEMA ) {
    /// Se añade el nuevo tema a travez de su clase
    $agregar = new tema_model();
    $agregar->set_nombre($_POST['tema']);
    $agregar->set_contenido($_POST['contenido']);
    $agregar->set_id_foro($_GET['id_foro']);
    $agregar->set_id_user_d($_SESSION['id_user']);
    $agregar->set_id_user_u($_SESSION['id_user']);
    $agregar->set_fecha_creacion(date("Y-m-d"));
    $agregar->set_fecha_ultimo(date("Y-m-d H:i:s"));
    if (isset($_POST['anuncio']) && $_POST['anuncio'] == true) {
      $agregar->set(true); // Agrega el tema a la base de datos
    } else {
      $agregar->set(); // Agrega el tema a la base de datos
    }
    $last_id = $agregar->get_last_id();
    // Agrega el nuevo tema a la tabla foro
    $__FORO->set_id_foro($_GET['id_foro']);
    $__FORO->set_id_ultimo_tema($last_id);
    $__FORO->set_nom_ultimo_tema($_POST['tema']);
    $__FORO->update_nuevo_tema();
    $__FORO->ALL_FORO(true);

    $url->set_id_2($_GET['id_foro']);
    $url->set_titulo($_POST['tema']);
    $url->set_id_1($last_id);
    header('location: tema/'.$url->url_amigable());
}else {
  header('location: index.php?view=tema&pagina_agregar&id_foro='.$_GET['id_foro']);
}
?>
