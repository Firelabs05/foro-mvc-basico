<?php
if (isset($_POST['contenido']) && strlen($_POST['contenido']) >= CANTIDAD_RESPUESTA) {
    /// Se edita el nuevo tema a travez de su clase
    $responder = new responder_model();
    $responder->set_id_user($_SESSION['id_user']);
    $responder->set_contenido($_POST['contenido']);
    $responder->set_id_tema($_GET['id_tema']);
    $responder->set_fecha_crea(date("Y-m-d H:i:s"));
    $responder->set();

    $tema->update_respuesta();
    $__USER->set_id_user($_SESSION['id_user']);
    $__USER->update_respuesta();

    $__FORO->set_id_foro($_GET['id_foro']);
    $__FORO->update_respuesta();

    $url->set_id_2($_GET['id_foro']);
    $url->set_titulo($data[0]['nombre']);
    $url->set_id_1($_GET['id_tema']);
    header('location: tema/'.$url->url_amigable());
}else {
  header('location: index.php?view=tema&mode=responder_tema&id_foro='.$_GET['id_foro'].'&id_tema='.$_GET['id_tema']);
}
?>
