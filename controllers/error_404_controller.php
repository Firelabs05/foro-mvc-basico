<?php
// Se agrega el DICCIONARIO DEL ERROR
$DICCIONARIO_ALERTA['TITULO'] = 'No existe la pagina';
$DICCIONARIO_ALERTA['SUBTITULO'] = 'ERROR 404';
$DICCIONARIO_ALERTA['CONTENIDO'] = 'Nuestros esclavos tubieron una revuelta y
                                  se han robado la pagina, lo sentimos';
// Se empieza a crear la PAGINA
$html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);
// Se pone el nav adecuado al tipo de usuario
$html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
// Se añade el contenido del ERROR
$DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(ERROR_PAGINA_LLENO,$DICCIONARIO_ALERTA);
// Se añade el contenido del index
$html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
// Se añade el footer
$html .= $__TEMPLATE->display_footer(FOOTER);
// Se imprime
echo $html;
 ?>
