<?php
if(array_key_exists($_GET['id_foro'],$_SESSION['foro'])){
  if (!isset($_GET['mode'])) {
    $_GET['mode'] = "pagina_todas";
  }
  // Se añaden los objetos
  $tema = new tema_model();
  if(isset($_GET['id_tema'])){
    $tema->set_id_tema($_GET['id_tema']);
  }
  $contenido = new class_display_tema();
  $url =new class_url_amigable();
  $url->set_id_1($_GET['id_foro']);
  $url->set_titulo($_SESSION['foro'][$_GET['id_foro']]['nombre']);
  // Se Añade la informacion a los DICCIONARIOS
  #### DICCIONARIOS DE LA MIJA
  $DICCIONARIO_MIJA +=[
    $_SESSION['foro'][$_GET['id_foro']]['nombre'] => [
      0 => 'tema_mija.png',
      1 => 'foro/'.$url->url_amigable()
    ]
  ];
  $url->reset();
  #### DICCIONARIO DEL CONTENIDO
  $DICCIONARIO_CONTENIDO['ID_FORO'] = $_GET['id_foro'];
  // Se empieza a crear la PAGINA añade la cabecera
  $html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);

  // Se añaden unas comprobaciones que se realizaran
  $admin_mod = (isset($_SESSION['id_user']) && $_SESSION['users'][$_SESSION['id_user']]['permiso'] > 0);
  $foro_estado = ($_SESSION['foro'][$_GET['id_foro']]['estado'] == 1);
  switch ($_GET['mode']){
    case 'pagina_agregar':
    if($admin_mod || $foro_estado){
      // Se crea el diccionario de las mijas
      $DICCIONARIO_MIJA +=[
        'Agregar Tema' => [
          0 => 'tema_nuevo_mija.png',
          1 => ''
        ]
      ];
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      // Se pone el nav del administrador
      $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
      // Se verifica si se a mandado informacion
      if($_POST){
        include ('controllers/tema/agregar_tema_controller.php');
      }else{
        if ($admin_mod) {
          $DICCIONARIO_CONTENIDO['ANUNCIO'] = $contenido->dinamic_check_anuncio();
        }else {
          $DICCIONARIO_CONTENIDO['ANUNCIO'] = '';
        }
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_TEMA,$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
    }else {
      header('location: index.php?view=index');
    }
    break;
    case 'pagina_editar':
    $tema->get_tema_id();
    $data = $tema->get_data();
    // No es lo adecuado pero funciona (?)
    if($admin_mod || $data[0]['id_user_d'] === $_SESSION['id_user']){
      // Se crea el diccionario de las mijas
      $url->set_id_1($_GET['id_tema']);
      $url->set_id_2($_GET['id_foro']);
      $url->set_titulo($data[0]['nombre']);
      $DICCIONARIO_MIJA +=[
        $data[0]['nombre'] => [
          0 => 'tema_mija.png',
          1 => 'tema/'.$url->url_amigable()
        ]
      ];
      $url->reset();
      $DICCIONARIO_MIJA +=[
        'Editar Tema' => [
          0 => 'tema_nuevo_mija.png',
          1 => ''
        ]
      ];
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      // Se pone el nav del administrador
      $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
      // Se verifica si se a mandado informacion
      if($_POST){
        include ('controllers/tema/editar_tema_controller.php');
      }else{
        $DICCIONARIO_CONTENIDO['TEMA'] =$data[0]['nombre'];
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $data[0]['contenido'];
        $DICCIONARIO_CONTENIDO['ID_TEMA'] = $data[0]['id_tema'];
        $DICCIONARIO_CONTENIDO['ID_FORO'] = $data[0]['id_foro'];
        if ($admin_mod) {
          if($data[0]['tipo'] == 1){
            $DICCIONARIO_CONTENIDO['ANUNCIO'] = $contenido->dinamic_check_anuncio(true);
          }else {
            $DICCIONARIO_CONTENIDO['ANUNCIO'] = $contenido->dinamic_check_anuncio();
          }
        }else {
          $DICCIONARIO_CONTENIDO['ANUNCIO'] = '';
        }
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(EDITAR_TEMA,$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
    }else {
      // Debe de regresar al foro
      $url->set_id_1($_GET['id_foro']);
      $url->set_titulo($_SESSION['foro'][$_GET['id_foro']]['nombre']);
      header('location: foro/'.$url->url_amigable());
    }
    break;
    case 'pagina_eliminar':
    $tema->get_tema_id();
    $data = $tema->get_data();
    if($admin_mod || $_SESSION['id_user'] == $data[0]['id_user_d']){
      $respuesta = new responder_model();
      $respuesta->set_id_tema($_GET['id_tema']);
      $respuesta->get_respuesta_users();
      $data_user = $respuesta->get_data();
      $respuesta->set_data();
      $tema->delete();
      foreach ($data_user as $key => $value) {
        foreach ($value as $key2 => $value2) {
          $respuesta->set_id_user($value2);
          $respuesta->get_mensajes_user();
          $cantidad = $respuesta->get_data();
          $__USER->set_id_user($value2);
          $respuesta->set_data();
          $__USER->set_mensajes($cantidad[0]['mensajes']);
          $__USER->update_mensajes();
        }
      }
      $tema->set_data();
      $tema->get_ultimo_tema();
      $data = $tema->get_data();
      $__FORO->set_id_foro($_GET['id_foro']);
      if(count($data) > 0){
        $__FORO->set_id_ultimo_tema($data[0]['id_tema']);
        $__FORO->set_nom_ultimo_tema($data[0]['nombre']);
        $__FORO->update_eliminar_tema(true);
      } else {
        $__FORO->set_nom_ultimo_tema('NULL');
        $__FORO->update_eliminar_tema();
      }
      $__USER->ALL_USERS(true);
      $__FORO->ALL_FORO(true);
      // Se crea la url dinamica
      $url->set_id_1($_GET['id_foro']);
      $url->set_titulo($_SESSION['foro'][$_GET['id_foro']]['nombre']);
      // Regresa foro
      header('location: foro/'.$url->url_amigable());
    }else {
      // Se crea la url dinamica
      $url->set_id_1($_GET['id_tema']);
      $url->set_id_2($_GET['id_foro']);
      $url->set_titulo($data[0]['nombre']);
      // Regresa al tema
      header('location: tema/'.$url->url_amigable());
    }
    break;
    case 'pagina_tema':
      // Se obtiene la informacion sobre el tema a mostrar
      $tema->get_tema_id();
      $data = $tema->get_data();
      // Se comprueba que en verdad exista
      if(count($data) > 0){
        // Se crea el url amigable para la mija
        $url->set_id_1($_GET['id_tema']);
        $url->set_id_2($_GET['id_foro']);
        $url->set_titulo($data[0]['nombre']);
        // Se agrega el diccionario de las MIJAS
        $DICCIONARIO_MIJA +=[
          $data[0]['nombre'] => [
            0 => 'tema_mija.png',
            1 => 'tema/'.$url->url_amigable()
          ]
        ];
        // Se agregan las mijas
        $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
        // Se hace la comprobacion del usuario
        if (isset($_SESSION['id_user'])){
          $DICCIONARIO_NAV['BOTON'] = '<div class="card border-success mb-3">
            <div class="card-body">
              <h4 class="card-title">
                Acciones
              </h4>
              <div class="btn-group" role="group" aria-label="Basic example">';
          if ($_SESSION['id_user'] == $data[0]['id_user_d']
            || $admin_mod) {
              // Estos no importa si esta habierto o cerrado
            $DICCIONARIO_NAV['BOTON'] .= $contenido->dinamic_boton_eliminar($_GET['id_foro'],$_GET['id_tema']);
            $DICCIONARIO_NAV['BOTON'] .= $contenido->dinamic_boton(
              '?view=tema&mode=pagina_editar&id_foro='.$_GET['id_foro'].'&id_tema='.$_GET['id_tema'],
              'Editar');
            }
          if ($data[0]['estado'] == 1){
            # Se añade el boton responder al diccionario
            $DICCIONARIO_NAV['BOTON'] .= $contenido->dinamic_boton(
              '?view=tema&mode=responder_tema&id_foro='.$_GET['id_foro'].'&id_tema='.$_GET['id_tema'],
              'Responder');
            if ($_SESSION['id_user'] == $data[0]['id_user_d']
              || $admin_mod) {
                # Se añade el boton cerrar al diccionario
                $DICCIONARIO_NAV['BOTON'] .= $contenido->dinamic_boton(
                  '?view=tema&mode=cerrar_tema&id_foro='.$_GET['id_foro'].'&id_tema='.$_GET['id_tema'].'&estado=0',
                  'Cerrar');
            }
          } else {
            if ($admin_mod) {
              # Se añade el boton abrir al diccionario
              $DICCIONARIO_NAV['BOTON'] .= $contenido->dinamic_boton(
                '?view=tema&mode=cerrar_tema&id_foro='.$_GET['id_foro'].'&id_tema='.$_GET['id_tema'].'&estado=1',
                'Abrir');
            }
          }
          $DICCIONARIO_NAV['BOTON'] .='
            </div>
          </div>
        </div>';
        }

        // Se agrega el nav
        $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
        // Se agrega el contenido
        $DICCIONARIO_CONTENIDO['TITULO'] = $data[0]['nombre'];
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__->BBcode($data[0]['contenido']);
        $DICCIONARIO_CONTENIDO['FIRMA'] = $__->BBcode($_SESSION['users'][$data[0]['id_user_d']]['firma']);
        $DICCIONARIO_CONTENIDO['USER'] = $_SESSION['users'][$data[0]['id_user_d']]['user'];
        $DICCIONARIO_CONTENIDO['IMAGEN'] = 'view/img/user_perfil/'.$_SESSION['users'][$data[0]['id_user_d']]['img'];
        $DICCIONARIO_CONTENIDO['TIPO'] = $__->__USER_TIP($_SESSION['users'][$data[0]['id_user_d']]['permiso']);
        $DICCIONARIO_CONTENIDO['RANGO'] = $__->__USER_RANG($_SESSION['users'][$data[0]['id_user_d']]['rango']);
        if($__->__USER_ACT($_SESSION['users'][$data[0]['id_user_d']]['ultima_con'])){
          $imagen_online = 'view/img/online.png';
        }else {
          $imagen_online = 'view/img/offline.png';
        }
        $DICCIONARIO_CONTENIDO['IMAGEN_ONLINE'] = $imagen_online;
        $DICCIONARIO_CONTENIDO['ID_TEMA'] = $_GET['id_tema'];
        $DICCIONARIO_CONTENIDO['RESPUESTAS'] = $contenido->dinamic_respuestas($_GET['id_tema'],$_GET['id_foro']);
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(MOSTRAR_TEMA,$DICCIONARIO_CONTENIDO);
        $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
        $tema->update_visitas();
      }else {
        header('location: index.php?view=index');
      }
    break;
    case 'responder_tema':
    $tema->get_tema_id();
    $data = $tema->get_data();
    if($foro_estado && count($data) > 0 && isset($_SESSION['id_user'])){
      // Se crea el diccionario de las mijas
      $url->set_id_1($_GET['id_tema']);
      $url->set_id_2($_GET['id_foro']);
      $url->set_titulo($data[0]['nombre']);
      $DICCIONARIO_MIJA +=[
        $data[0]['nombre'] => [
          0 => 'tema_mija.png',
          1 => 'tema/'.$url->url_amigable()
        ]
      ];
      $url->reset();
      $DICCIONARIO_MIJA +=[
        'Responder Tema' => [
          0 => 'tema_nuevo_mija.png',
          1 => ''
        ]
      ];
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      // Se pone el nav del administrador
      $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
      // Se verifica si se a mandado informacion
      if($_POST){
        include ('controllers/tema/responder_tema_controller.php');
      }else{
        $DICCIONARIO_CONTENIDO['ID_FORO'] = $_GET['id_foro'];
        $DICCIONARIO_CONTENIDO['ID_TEMA'] = $_GET['id_tema'];
        $DICCIONARIO_CONTENIDO['TITULO'] = $data[0]['nombre'];
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(RESPONDER_TEMA,$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
    }else {
      // Se crea la url dinamica
      $url->set_id_1($_GET['id_foro']);
      $url->set_titulo($_SESSION['foro'][$_GET['id_foro']]['nombre']);
      // regresa foro
      header('location: foro/'.$url->url_amigable());
    }
    break;
    case 'editar_respuesta':
    $tema->get_tema_id();
    $data_tema = $tema->get_data();
    // Se crea el diccionario de las mijas
    $url->set_id_1($_GET['id_tema']);
    $url->set_id_2($_GET['id_foro']);
    $url->set_titulo($data_tema[0]['nombre']);
    if (isset($_GET['id_respuesta']) && !empty($_GET['id_respuesta'])
        && $foro_estado) {
      $respuesta = new responder_model();
      $respuesta->set_id_respuesta($_GET['id_respuesta']);
      $respuesta->get_respuesta();
      $data = $respuesta->get_data();
      if(count($data) > 0 ){
        if (isset($_SESSION['id_user'])
        && ($_SESSION['id_user'] == $data[0]['id_user']
        || $admin_mod)) {
          $DICCIONARIO_MIJA +=[
            $data_tema[0]['nombre'] => [
              0 => 'tema_mija.png',
              1 => 'tema/'.$url->url_amigable()
            ]
          ];
          $url->reset();
          $DICCIONARIO_MIJA +=[
            'Responder Tema' => [
              0 => 'tema_nuevo_mija.png',
              1 => ''
            ]
          ];
          $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
          // Se pone el nav del administrador
          $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
          // Se verifica si se a mandado informacion
          if($_POST){
            include ('controllers/tema/editar_respuesta_tema_controller.php');
          }else{
            $DICCIONARIO_CONTENIDO['ID_FORO'] = $_GET['id_foro'];
            $DICCIONARIO_CONTENIDO['ID_TEMA'] = $_GET['id_tema'];
            $DICCIONARIO_CONTENIDO['ID_RESPUESTA'] = $data[0]['id_respuesta'];
            $DICCIONARIO_CONTENIDO['TITULO'] = $data_tema[0]['nombre'];
            $DICCIONARIO_CONTENIDO['CONTENIDO'] = $data[0]['contenido'];
            $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(EDITAR_RESPUESTA_TEMA,$DICCIONARIO_CONTENIDO);
          }
          $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
        }else {
          // Se crea la url dinamica
          $url->set_id_1($_GET['id_foro']);
          $url->set_titulo($_SESSION['foro'][$_GET['id_foro']]['nombre']);
          // regresa foro
          header('location: foro/'.$url->url_amigable());
        }
      }else {
        header('location: foro/'.$url->url_amigable());
      }
    }else {
      header('location: foro/'.$url->url_amigable());
    }
    break;
    case 'eliminar_respuesta':
    $respuesta = new responder_model();
    $respuesta->set_id_respuesta($_GET['id_respuesta']);
    $respuesta->get_respuesta();
    $data = $respuesta->get_data();
    $tema->get_tema_id();
    $data_tema = $tema->get_data();
    $url->set_id_1($_GET['id_tema']);
    $url->set_id_2($_GET['id_foro']);
    $url->set_titulo($data_tema[0]['nombre']);
    // No es lo adecuado pero funciona (?)
    if($admin_mod || $data[0]['id_user'] === $_SESSION['id_user']){
      $__USER->set_id_user($data[0]['id_user']);
      $__USER->update_eliminar_respuesta();
      $__USER->ALL_USERS(true);
      $__FORO->set_id_foro($_GET['id_foro']);
      $__FORO->update_eliminar_respuesta();
      $tema->update_eliminar_respuesta();
      $respuesta->delete();
      header('location: tema/'.$url->url_amigable());
    }else {
      header('location: tema/'.$url->url_amigable());
    }
    break;
    case 'cerrar_tema':
      // Agregar comprobaciones para que pueda cerrarlo
      if (isset($_GET['estado']) && in_array($_GET['estado'], [1,0])){
        $tema->get_tema_id();
        $data = $tema->get_data();
        $url->set_id_1($_GET['id_tema']);
        $url->set_id_2($_GET['id_foro']);
        $url->set_titulo($data[0]['nombre']);
        if (count($data)>0 && ($admin_mod
          || $_SESSION['id_user'] == $data[0]['id_user_d'])) {
          $tema->set_estado($_GET['estado']);
          $tema->update_estado();
          header('location: tema/'.$url->url_amigable());
        } else {
          header('location: tema/'.$url->url_amigable());
        }
      }else {
        // Se crea la url dinamica
        $url->set_id_1($_GET['id_foro']);
        $url->set_titulo($_SESSION['foro'][$_GET['id_foro']]['nombre']);
        // regresa foro
        header('location: foro/'.$url->url_amigable());
      }
    break;
    default:
      // Se pone el nav
      if (isset($_SESSION['id_user'])){
        if ($admin_mod || $foro_estado) {
          # Se añade el boton al diccionario
          $DICCIONARIO_NAV['BOTON'] = $contenido->dinamic_boton(
            '?view=tema&mode=pagina_agregar&id_foro='.$_GET['id_foro'],
            'Nueva tema');
        }
      }
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
      $DICCIONARIO_CONTENIDO['CONTENIDO'] = $contenido->dinamic_temas($_GET['id_foro']);
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
    break;
  }
  $html .= $__TEMPLATE->display_footer(FOOTER);
  echo $html;
} else {
  // Regresa al index
  header('location: ../index.php?view=index');
}
?>
