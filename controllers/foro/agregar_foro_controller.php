<?php
if (isset($_POST['categoria']) && isset($_POST['descripcion'])
    && isset($_POST['foro']) && isset($_POST['estado_foro'])
    && array_key_exists($_POST['categoria'],$_SESION['category'])
    && strlen($_POST['foro']) >= CANTIDAD_FORO
    && strlen($_POST['descripcion']) >= CANTIDAD_FORO_DESCRIPCION) {

    // Se añade el nuevo foro a travez de su clase
    $__FORO->set_nombre($_POST['foro']);
    $__FORO->set_estado($_POST['estado_foro']);
    $__FORO->set_descripcion($_POST['descripcion']);
    $__FORO->set_id_categoria($_POST['categoria']);
    $__FORO->set(); // Agrega el foro a la base de datos

    // Agrega un alerta de success que si se pudo crear el foro
    $DICCIONARIO_ALERTA['TITULO'] = 'Foro creado';
    $DICCIONARIO_ALERTA['CONTENIDO'] = 'Todo salio bien tenemos a nuestros esclavos trabajando';
    $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(SUCCESS,$DICCIONARIO_ALERTA);
    $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_FORO,$DICCIONARIO_CONTENIDO);
} else {
  // Agrega un alerta de error que no se pudo crear el foro
  $DICCIONARIO_ALERTA['TITULO'] = 'Por favor rellene los campos';
  $DICCIONARIO_ALERTA['CONTENIDO'] = 'Para poder proseguir debe de rellenar los campos adecuadamente';
  $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
  $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_FORO,$DICCIONARIO_CONTENIDO);
}
?>
