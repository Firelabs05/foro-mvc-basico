<?php
if (isset($_POST['categoria']) && isset($_POST['descripcion'])
    && isset($_POST['foro']) && isset($_POST['estado_foro'])
    && strlen($_POST['foro']) >= CANTIDAD_FORO
    && strlen($_POST['descripcion']) >= CANTIDAD_FORO_DESCRIPCION
    && array_key_exists($_POST['categoria'],$_SESSION['category'])){
    // Se edita el foro a travez de su clase
    $__FORO->set_id_foro($_GET['id_foro']);
    $__FORO->set_nombre($_POST['foro']);
    $__FORO->set_estado($_POST['estado_foro']);
    $__FORO->set_descripcion($_POST['descripcion']);
    $__FORO->set_id_categoria($_POST['categoria']);
    $__FORO->update(); // edita el foro en la base de datos
    $__FORO->ALL_FORO(true);

    header('location: ?view=foro&foro=pagina_todas');
} else {
  header ('location: ?view=foro&foro=pagina_editar&id_foro='.$_POST['id_foro']);
}
?>
