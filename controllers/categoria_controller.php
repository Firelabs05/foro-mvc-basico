<?php
if(isset($_SESSION['id_user'])
  && array_key_exists($_SESSION['id_user'],$_SESSION['users'])
  && $_SESSION['users'][$_SESSION['id_user']]['permiso'] == 2){
    ################# MUESTRA LAS PAGINAS PARA REALIZAR LAS ACCIONES ###################
    if (!isset($_GET['categoria'])) {
      $_GET['categoria'] = "pagina_todas";
    }
    // Se Añade la informacion a los DICCIONARIOS
    #### DICCIONARIOS DEL NAV
    $DICCIONARIO_NAV['ID_USER'] = $_SESSION['id_user'];
    $DICCIONARIO_NAV['NOMBRE_USER'] = $_SESSION['users'][$_SESSION['id_user']]['user'];

    #### DICCIONARIOS DE LA MIJA
    $DICCIONARIO_MIJA +=[
      'Gestionar categorias' => [
        0 => 'tema_mija.png',
        1 => '?view=categoria&categoria=pagina_todas'
      ]
    ];

    // Se empieza a crear la PAGINA añade la cabecera
    $html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);

    // Se utiliza un switch para analizar cual parte de la pagina se presentara
    switch ($_GET['categoria']) {
      case 'pagina_agregar':
      // Se crea el diccionario de las mijas
      $DICCIONARIO_MIJA +=[
        'Agregar categoria' => [
          0 => 'tema_nuevo_mija.png',
          1 => '?view=categoria&categoria=pagina_agregar'
        ]
      ];
      // Se añade el nav de forma manual
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      $html .= $__TEMPLATE->display_nav_manual(NAV_ADMIN_AGREGAR_CAT,$DICCIONARIO_NAV);
      // Se añade el contenido de la pagina
      if($_POST){
        include ('controllers/categoria/agregar_categoria_controller.php');
      }else{
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_CATEGORIA,$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
      break;
      case 'pagina_editar':
      if(isset($_GET['id_categoria'])
      && array_key_exists($_GET['id_categoria'],$_SESSION['category'])){
        // Se crea el diccionario de las mijas
        $DICCIONARIO_MIJA +=[
          'Editar categoria' => [
            0 => 'tema_nuevo_mija.png',
            1 => '?view=categoria&categoria=pagina_editar'
          ]
        ];
        // Se añade el nav de forma manual
        $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
        $html .= $__TEMPLATE->display_nav_manual(NAV_ADMIN_TODOS_CAT,$DICCIONARIO_NAV);
        // Se añade el contenido
        if($_POST){
          include ('controllers/categoria/editar_categoria_controller.php');
        }else {
          // Se añade la categoria a modificar al $DICCIONARIO_CONTENIDO
          $DICCIONARIO_CONTENIDO += [
            'ID_CATEGORIA'=> $_GET['id_categoria'],
            'CATEGORIA'=> $_SESSION['category'][$_GET['id_categoria']]['nombre'],
            'DESCRIPCION'=> $_SESSION['category'][$_GET['id_categoria']]['descripcion']
          ];
          $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(EDITAR_CATEGORIA,$DICCIONARIO_CONTENIDO);
        }
        $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
      }else{
        header('location: ?view=categoria&categoria=pagina_todas');
      }
      break;
      default:
      // Se pone el nav del administrador
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      $html .= $__TEMPLATE->display_nav_manual(NAV_ADMIN_TODOS_CAT,$DICCIONARIO_NAV);
      // Se añade el contenido
      if(isset($_GET['id_categoria'])
        && array_key_exists($_GET['id_categoria'],$_SESSION['category'])){
        include ('controllers/categoria/borrar_categoria_controller.php');
      }else{
        // Se añaden los objetos
        $contenido = new class_display_categorias();
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido($contenido->dinamic_categoria(),$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
      break;
    }
    // Se añade el footer
    $html .= $__TEMPLATE->display_footer(FOOTER);
    // Se imprime
    echo $html;
} else {
  header('location: ?view=index');
}
?>
