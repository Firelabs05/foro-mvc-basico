<?php
if(!isset($_SESSION['id_user'])){
  if(isset($_GET['keypass']) && !empty($_GET['keypass'])) {
    // Se empieza a crear la plantilla el head
    $html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);

    // Se crea el nav
    $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
    $__USER->set_keypass($_GET['keypass']);
    $__USER->get_recuperar_keypass();
    $data = $__USER->get_data();

    if(count($data) > 0){
      $__USER->set_newpass($data[0]['newpass']);
      $__USER->set_password(md5($data[0]['newpass']));
      $__USER->set_id_user($data[0]['id_user']);
      $__USER->set_email($data[0]['email']);
      $__USER->set_user($data[0]['user']);

      $mail = new PHPMailer\PHPMailer\PHPMailer();    // Passing `true` enables exceptions

      //Server settings
      $mail->CharSet = 'UTF-8';
      $mail->isSMTP();                                // Set mailer to use SMTP
      $mail->Host = PHPMAILER_HOST;                   // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                         // Enable SMTP authentication
      $mail->Username = PHPMAILER_USER;               // SMTP username
      $mail->Password = PHPMAILER_PASS;               // SMTP password
      $mail->SMTPSecure = 'ssl';                      // Enable TLS encryption, `ssl` also accepted
      $mail->Port = PHPMAILER_PORT;                   // TCP port to connect to

      //Recipients
      $mail->setFrom(PHPMAILER_USER, TITLE);
      $mail->addAddress($__USER->get_email(), $__USER->get_user());     // Add a recipient

      //Content
      $mail->isHTML(true);                            // Set email format to HTML
      $mail->Subject = 'Su nueva contraseña '.TITLE;

      $link = BASE_URL.'index.php?view=index';

      $DICCIONARIO_EMAIL['USER'] = $__USER->get_user();
      $DICCIONARIO_EMAIL['LINK'] = $link;
      $DICCIONARIO_EMAIL['FECHA'] = date("Y-m-d H:i:s");
      $DICCIONARIO_EMAIL['NEWPASS'] = $__USER->get_newpass();

      $mail->Body    = $__TEMPLATE->display_contenido(EMAIL_RECUPERAR_PASS_NEWPASS,$DICCIONARIO_EMAIL);
      $mail->AltBody = 'Hola '.$__USER->get_user(). 'Esta es su nueva contraseña:'.$__USER->get_newpass().'
                        gracias por su paciencia, ahora intenta logearte, recuerda que siempre, puedes cambiarla
                        en tus propiedades'.$link;

      if(!$mail->send()) {
        $DICCIONARIO_ALERTA['TITULO'] = 'No se pudo enviar el correo con su nuevo password';
        $DICCIONARIO_ALERTA['SUBTITULO'] = 'Error';
        $DICCIONARIO_ALERTA['CONTENIDO'] = 'No podemos conectarnos con el servidor,
                                             por favor comunicate con un administrador';
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(ERROR_PAGINA_LINEAL,$DICCIONARIO_ALERTA);
      }else{
        $__USER->update_password();
        $DICCIONARIO_ALERTA['TITULO'] = 'Se envio el correo con su nuevo password';
        $DICCIONARIO_ALERTA['SUBTITULO'] = 'Todo salio bien';
        $DICCIONARIO_ALERTA['CONTENIDO'] = 'Se le a enviado un correo con su nuevo password,
                                             por favor revise su bandeja de entrada';
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(SUCCESS_PAGINA_LINEAL,$DICCIONARIO_ALERTA);

      }
    }else{
      header('location: ?view=error_404');
    }
  }else{
    header('location: ?view=error_404');
  }
}else{
  header('location: ?view=index');
}
// Se pasa el contenido
$html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
$html .= $__TEMPLATE->display_footer(FOOTER);
echo $html;
 ?>
