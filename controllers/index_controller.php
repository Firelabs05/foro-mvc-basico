<?php
$contenido = new class_display_index();
// Se le pasa el contenido al DICCIONARIO
$DICCIONARIO_CONTENIDO['CONTENIDO'] = $contenido->dinamic_index();
// Se empieza a crear la PAGINA añade la cabecera
$html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);
// Se pone el nav adecuado al tipo de usuario
$html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
// Se añade el contenido del index
$html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
// Se añade el footer
$html .= $__TEMPLATE->display_footer(FOOTER);
// Se imprime
echo $html;
?>
