<?php
// Se empieza a crear la PAGINA
$html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);
if (isset($_GET['keyreg']) && !empty($_GET['keyreg'])) {
  $html .= $__TEMPLATE->display_nav($DICCIONARIO_NAV);
  if (isset($_SESSION['id_user'])
    && array_key_exists($_SESSION['id_user'],$_SESSION['users'])){
      $__USER->set_keyreg($_GET['keyreg']);
      $__USER->set_id_user($_SESSION['id_user']);
      // Revisa si el keyreg corresponde al usuario logueado
      $__USER->get_activacion();

      if(count($__USER->get_data()) == 1){
        $__USER->set_activo(1);
        $__USER->set_keyreg("");
        // Activa el usuario
        $__USER->update_activacion();

        $DICCIONARIO_ALERTA['TITULO'] = 'Activacion';
        $DICCIONARIO_ALERTA['SUBTITULO'] = 'Cuenta Activada';
        $DICCIONARIO_ALERTA['CONTENIDO'] = 'Ahora puedes utilizar todos los servicios
                                            brindados por el foro, por favor se educado
                                            con los demas cibernautas, todos para uno y
                                            uno para todos<br> Disfruta de nuestro foro
                                            <a class ="alert-link" href = "?view=index">
                                              Haz click aqui
                                            </a>';
        // Se añade el contenido de la alerta
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(SUCCESS_PAGINA_LINEAL,$DICCIONARIO_ALERTA);
      } else {
        $DICCIONARIO_ALERTA['TITULO'] = 'Error en la activacion';
        $DICCIONARIO_ALERTA['SUBTITULO'] = 'Cuenta no vinculada';
        $DICCIONARIO_ALERTA['CONTENIDO'] = 'No se encontro la cuenta vinculada,
                                            puede que ya haya sido activada previamente
                                            de no ser asi,<br>
                                            por favor, comuniquese con un administrador';
        // Se añade el contenido del ERROR
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(ERROR_PAGINA_LINEAL,$DICCIONARIO_ALERTA);
      }
  } else {
      // Se agrega el DICCIONARIO DEL ERROR
      $DICCIONARIO_ALERTA['TITULO'] = 'No esta logueado';
      $DICCIONARIO_ALERTA['SUBTITULO'] = 'DEBE LOGEARSE';
      $DICCIONARIO_ALERTA['CONTENIDO'] = 'Para poder proseguir debe de logearse';
      // Se añade el contenido del ERROR
      $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(ERROR_PAGINA_LINEAL,$DICCIONARIO_ALERTA);
    }
} else {
  header("location: ?view=error_404");
}
// Se añade el contenido del index
$html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
// Se añade el footer
$html .= $__TEMPLATE->display_footer(FOOTER);
// Se imprime
echo $html;
?>
