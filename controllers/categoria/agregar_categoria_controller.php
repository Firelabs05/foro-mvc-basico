<?php
if (isset($_POST['categoria']) && isset($_POST['descripcion'])
  && strlen($_POST['categoria']) >= CANTIDAD_CATEGORIA
  && strlen($_POST['descripcion']) >= CANTIDAD_CATEGORIA_DESCRIPCION) {
    /// Se añade la nueva categoria a travez de su clase
    $__CATEGORIA->set_nombre($_POST['categoria']);
    $__CATEGORIA->set_descripcion($_POST['descripcion']);
    $__CATEGORIA->set(); // Agrega la categoria a la base de datos

    // Agrega un alerta de success que si se pudo crear la categoria
    $DICCIONARIO_ALERTA['TITULO'] = 'Categoria creada';
    $DICCIONARIO_ALERTA['CONTENIDO'] = 'Todo salio bien tenemos a nuestros esclavos trabajando';
    $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(SUCCESS,$DICCIONARIO_ALERTA);
    $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_CATEGORIA,$DICCIONARIO_CONTENIDO);
} else {
  // Agrega un alerta de error que no se pudo crear la categoria
  $DICCIONARIO_ALERTA['TITULO'] = 'Por favor rellene los campos';
  $DICCIONARIO_ALERTA['CONTENIDO'] = 'Para poder proseguir debe de rellenar los campos adecuadamente';
  $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
  $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_CATEGORIA,$DICCIONARIO_CONTENIDO);
}
?>
