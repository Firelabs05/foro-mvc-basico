<?php
if(isset($_POST['categoria']) && isset($_POST['descripcion'])
  && strlen($_POST['categoria'])) >= CANTIDAD_CATEGORIA
  && strlen($_POST['descripcion']) >= CANTIDAD_CATEGORIA_DESCRIPCION) {

  /// Se modifica la categoria a travez de su clase
  $__CATEGORIA->set_id_categoria($_POST['id_categoria']);
  $__CATEGORIA->set_nombre($_POST['categoria']);
  $__CATEGORIA->set_descripcion($_POST['descripcion']);
  $__CATEGORIA->update(); // Edita la categoria en la base de datos
  $__CATEGORIA->ALL_CATEGORY(true); // Se actualiza la variable de $_SESSION['category']

  header ('location: ?view=categoria&categoria=pagina_todas'); // Regresa a gestionar categorias
} else {
  // Regresa a editar categoria
  header ('location: ?view=categoria&categoria=pagina_editar&id_categoria='.$_POST['id_categoria']);
}
?>
