<?php
if(isset($_POST['email']) && !empty($_POST['email'])){
  $__USER->set_email($_POST['email']);
  $__USER->get_recuperar_password();
  $data = $__USER->get_data();
  if (count($data) > 0) {
    $__USER->set_keypass(md5(time()));
    $__USER->set_user($data[0]['user']);

    $mail = new PHPMailer\PHPMailer\PHPMailer();      // Passing `true` enables exceptions

    //Server settings
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();                                  // Set mailer to use SMTP
    $mail->Host = PHPMAILER_HOST;                     // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                           // Enable SMTP authentication
    $mail->Username = PHPMAILER_USER;                 // SMTP username
    $mail->Password = PHPMAILER_PASS;                 // SMTP password
    $mail->SMTPSecure = 'ssl';                        // Enable TLS encryption, `ssl` also accepted
    $mail->Port = PHPMAILER_PORT;                     // TCP port to connect to

    //Recipients
    $mail->setFrom(PHPMAILER_USER, TITLE);
    $mail->addAddress($__USER->get_email(), $__USER->get_user());     // Add a recipient

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Recuperacion de contraseña: '.TITLE;

    $link = BASE_URL.'index.php?view=keypass&keypass='.$__USER->get_keypass();

    $DICCIONARIO_EMAIL['USER'] = $__USER->get_user();
    $DICCIONARIO_EMAIL['LINK'] = $link;
    $DICCIONARIO_EMAIL['FECHA'] = date("Y-m-d H:i:s");
    $mail->Body    = $__TEMPLATE->display_contenido(EMAIL_RECUPERAR_PASS,$DICCIONARIO_EMAIL);
    $mail->AltBody = 'Hola '.$__USER->get_user(). 'Por favor de click en el siguiente enlace
                      si usted solicito la recuperacion de contraseña en caso de que no fuera asi
                      por favor ignore este correo'.$link;

    if(!$mail->send()) {
      $DICCIONARIO_ALERTA['TITULO'] = 'Error';
      $DICCIONARIO_ALERTA['CONTENIDO'] = 'No podemos conectarnos con el servidor,
                                          <br> por favor comunicate con un administrador';
      $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
    }else{
      $__USER->set_newpass(strtoupper(substr(md5(time()),0,8)));
      $__USER->set_id_user($data[0]['id_user']);
      $__USER->update_recuperar_password();

      $DICCIONARIO_ALERTA['TITULO'] = 'Todo salio bien';
      $DICCIONARIO_ALERTA['CONTENIDO'] = 'Se ha mandado un email a su correo por favor verificarlo';
      $html = $__TEMPLATE->display_contenido(SUCCESS,$DICCIONARIO_ALERTA);
    }
  }else {
    $DICCIONARIO_ALERTA['TITULO'] = 'Error';
    $DICCIONARIO_ALERTA['CONTENIDO'] = 'El email ingresado no existe';
    $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
  }
}else{
  $DICCIONARIO_ALERTA['TITULO'] = 'Error';
  $DICCIONARIO_ALERTA['CONTENIDO'] = 'Ingrese un email por favor';
  $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
}
echo $html;
?>
