<?php
if(isset($_POST['user']) && !empty($_POST['user'])
  && isset($_POST['password']) && !empty($_POST['password'])){

  // Busca si existe el usuario
  $__USER->set_user($_POST['user']);
  $__USER->set_email($_POST['user']);
  $__USER->get_login();
  $data = $__USER->get_data();

  // Comprueba si existe
  if (count($__USER->get_data()) > 0) {
    if ($data[0]['password'] === md5($_POST['password'])){
      if(isset($_POST['sesion'])){
        ini_set('session.cokie_lifetime',time() + (60*60*24));
      }
      $_SESSION['id_user'] = $data[0]['id_user'];
      $time = time();
      $__USER->set_ultima_con($time);
      $__USER->set_id_user($_SESSION['id_user']);
      $__USER->update_ultima_con();
      $__USER->ALL_USERS(true);
      $html = "1";
    } else {
      $DICCIONARIO_ALERTA['TITULO'] = 'Error';
      $DICCIONARIO_ALERTA['CONTENIDO'] = 'Contraseña incorrecta olvidaste tu contraseña?';
      $DICCIONARIO_ALERTA['MODAL'] = 'modal_recuperar_password';
      $DICCIONARIO_ALERTA['CONTENIDO_MODAL'] = 'Haz click aqui';
      $html = $__TEMPLATE->display_contenido(ERROR_A_MODAL,$DICCIONARIO_ALERTA);
    }
  }else{
    $DICCIONARIO_ALERTA['TITULO'] = 'Error';
    $DICCIONARIO_ALERTA['CONTENIDO'] = 'No existe el usuario quieres registrate?';
    $DICCIONARIO_ALERTA['MODAL'] = 'modal_registro';
    $DICCIONARIO_ALERTA['CONTENIDO_MODAL'] = 'Haz click aqui';
    $html = $__TEMPLATE->display_contenido(ERROR_A_MODAL,$DICCIONARIO_ALERTA);
  }
}else {
  $DICCIONARIO_ALERTA['TITULO'] = 'Error';
  $DICCIONARIO_ALERTA['CONTENIDO'] = 'Por favor rellene todos los campos';
  $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
}
echo $html;
?>
