<?php
if(isset($_POST['user']) && isset($_POST['password']) && isset($_POST['email'])
  && strlen($_POST['user']) >= CANTIDAD_USER
  && strlen($_POST['password']) >= CANTIDAD_PASSWORD){
  // Se busca si existe el usuario
  $__USER->set_user($_POST['user']);
  $__USER->set_email($_POST['email']);
  $__USER->get_login();

  if(count($__USER->get_data()) == 0){
    $__USER->set_keyreg(md5(time()));
    $mail = new PHPMailer\PHPMailer\PHPMailer();

    //Server settings
    $mail->isSMTP();                    // Set mailer to use SMTP
    $mail->Host = PHPMAILER_HOST;       // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;             // Enable SMTP authentication
    $mail->Username = PHPMAILER_USER;   // SMTP username
    $mail->Password = PHPMAILER_PASS;   // SMTP password
    $mail->SMTPSecure = 'ssl';          // Enable TLS encryption, `ssl` also accepted
    $mail->Port = PHPMAILER_PORT;       // TCP port to connect to

    //Recipients
    $mail->setFrom(PHPMAILER_USER, TITLE);
    $mail->addAddress($__USER->get_email(), $__USER->get_user());     // Add a recipient

    //Content
    $mail->isHTML(true);                                 // Set email format to HTML
    $mail->Subject = 'Activacion de tu cuenta de: '.TITLE;

    $link = BASE_URL.'index.php?view=activacion&keyreg='.$__USER->get_keyreg();

    $DICCIONARIO_EMAIL['USER'] = $__USER->get_user();
    $DICCIONARIO_EMAIL['LINK'] = $link;
    $DICCIONARIO_EMAIL['FECHA'] = date("Y-m-d H:i:s");
    $mail->Body    = $__TEMPLATE->display_contenido(EMAIL_ACTIVACION,$DICCIONARIO_EMAIL);
    $mail->AltBody = 'Hola '.$__USER->get_user(). 'Por favor acceda a este
                        enlace para la activacion de su cuenta: '.$link;

    if(!$mail->send()) {
      $DICCIONARIO_ALERTA['CONTENIDO'] = 'No podemos conectarnos con el servidor,
                                            <br> por favor comunicate con un administrador';
      $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
    }else {
      $__USER->set_password(md5($_POST['password']));
      $__USER->set_fecha_reg(date("Y-m-d"));
      $__USER->set();

      if ($__USER->get_last_id() != "") {
        $_SESSION['id_user'] = $__USER->get_last_id();
        $html= "1";
      }else{
        $DICCIONARIO_ALERTA['CONTENIDO'] = 'Hubo un fallo y desconocemos por que,
                                              <br>por favor comunicate con un administrador';
        $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
      }
    }
  }else{
    $user = $__USER->get_data();
    if($user[0]['user'] == $_POST['user']){
      $error = 'usuario';
    }else{
      $error = 'email';
    }
    $DICCIONARIO_ALERTA['TITULO'] = 'Error';
    $DICCIONARIO_ALERTA['CONTENIDO'] = 'El '.$error.' ya existe por favor ocupe otro';
    $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
  }
}else{
  $DICCIONARIO_ALERTA['CONTENIDO'] = 'Por favor rellene todos los campos';
  $html = $__TEMPLATE->display_contenido(ERROR,$DICCIONARIO_ALERTA);
}
echo $html;
?>
