<?php
if(isset($_SESSION['id_user'])
  && array_key_exists($_SESSION['id_user'],$_SESSION['users'])
  && $_SESSION['users'][$_SESSION['id_user']]['permiso'] == 2){
    ################# MUESTRA LAS PAGINAS PARA REALIZAR LAS ACCIONES ###################
    if (!isset($_GET['foro'])) {
      $_GET['foro'] = 'pagina_todas';
    }
    // Se Añade la informacion a los DICCIONARIOS
    #### DICCIONARIOS DEL NAV
    $DICCIONARIO_NAV['ID_USER'] = $_SESSION['id_user'];
    $DICCIONARIO_NAV['NOMBRE_USER'] = $_SESSION['users'][$_SESSION['id_user']]['user'];

    #### DICCIONARIOS DE LA MIJA
    $DICCIONARIO_MIJA +=[
      'Gestionar foros' => [
        0 => 'tema_mija.png',
        1 => '?view=foro&foro=pagina_todas'
      ]
    ];

    // Se empieza a crear la PAGINA añade la cabecera
    $html = $__TEMPLATE->display_contenido(HEAD,$DICCIONARIO_CABECERA);
    // Se utiliza un switch para analizar cual parte de la pagina se presentara
    switch ($_GET['foro']) {
      case 'pagina_agregar':
      // Se añaden los objetos
      $contenido = new class_display_foro();
      // Se crea el diccionario de las mijas
      $DICCIONARIO_MIJA +=[
        'Agregar foro' => [
          0 => 'tema_nuevo_mija.png',
          1 => '?view=foro&foro=pagina_agregar'
        ]
      ];
      // Se pone el nav del administrador
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      $html .= $__TEMPLATE->display_nav_manual(NAV_ADMIN_AGREGAR_FORO,$DICCIONARIO_NAV);
      // Se añade el contenido
      // Se añade el option dinamico
      $DICCIONARIO_CONTENIDO['OPTION'] = $contenido->dinamic_option();
      if($_POST){
        include ('controllers/foro/agregar_foro_controller.php');
      }else{
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(AGREGAR_FORO,$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
      break;
      case 'pagina_editar':
      if (isset($_GET['id_foro'])
        && array_key_exists($_GET['id_foro'],$_SESSION['foro'])) {
        // Se crea el diccionario de las mijas
        $DICCIONARIO_MIJA +=[
          'Editar foro' => [
            0 => 'tema_nuevo_mija.png',
            1 => '?view=foro&foro=pagina_editar'
          ]
        ];
        // Se añade el nav de forma manual
        $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
        $html .= $__TEMPLATE->display_nav_manual(NAV_ADMIN_TODOS_FORO,$DICCIONARIO_NAV);
        // Se añade el contenido
        if($_POST){
          include ('controllers/foro/editar_foro_controller.php');
        }else {
          // Se añaden los objetos
          $contenido = new class_display_foro();
          // Se añade la categoria a modificar al $DICCIONARIO_CONTENIDO
          $DICCIONARIO_CONTENIDO += [
            'ID_FORO'=> $_GET['id_foro'],
            'FORO'=> $_SESSION['foro'][$_GET['id_foro']]['nombre'],
            'DESCRIPCION'=> $_SESSION['foro'][$_GET['id_foro']]['descripcion'],
            'OPTION' => $contenido->dinamic_option($_SESSION['foro'][$_GET['id_foro']]['id_categoria']),
            'ESTADO' => $contenido->dinamic_estado($_SESSION['foro'][$_GET['id_foro']]['estado'])
          ];
          $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido(EDITAR_FORO,$DICCIONARIO_CONTENIDO);
        }
      } else {
        header('location: ?view=foro&foro=pagina_todas');
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
      break;
      default:
      // Se añaden los objetos
      $contenido = new class_display_foro();
      // Se pone el nav del administrador
      $DICCIONARIO_NAV['MIJAS'] = $__->dinamic_mijas($DICCIONARIO_MIJA);
      $html .= $__TEMPLATE->display_nav_manual(NAV_ADMIN_TODOS_FORO,$DICCIONARIO_NAV);

      // Se añade el contenido
      if(isset($_GET['id_foro'])
        && array_key_exists($_GET['id_foro'],$_SESSION['foro'])){
        include ('controllers/foro/borrar_foro_controller.php');
      }else{
        $DICCIONARIO_CONTENIDO['CONTENIDO'] = $__TEMPLATE->display_contenido($contenido->dinamic_foro(),$DICCIONARIO_CONTENIDO);
      }
      $html .= $__TEMPLATE->display_contenido(CONTENIDO,$DICCIONARIO_CONTENIDO);
      break;
    }
    // Se añade el footer
    $html .= $__TEMPLATE->display_footer(FOOTER);
    // Se imprime
    echo $html;
}else {
  header('location: ?view=index');
}
?>
