<?php
#####################SESSION#####################
session_start();
#######################FIN SESSION##################

######################### ZONA HORARIA DE LA APLICACION ####################
date_default_timezone_set('America/Mexico_City');
############################## FIN ZONA HORARIA ##############################

########################### LIBRERIAS ########################
#######COMPOSER#######
require_once('vendor/autoload.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

#PHPMAILER
const PHPMAILER_HOST = 'p3plcpnl0173.prod.phx3.secureserver.net';
const PHPMAILER_USER = 'public@ocrend.com';
const PHPMAILER_PASS = 'Prinick2016';
const PHPMAILER_PORT = 465;

############################# FIN LIBRERIAS ########################

############################ CLASES ##############################
######CLASES DE LAS PAGINAS######
require('models/libs/class_connections.php');
require('models/libs/class_user.php');
require('models/libs/class_categoria.php');
require('models/libs/class_foro.php');
require('models/libs/class_tema.php');
require('models/libs/class_respuesta.php');
require('models/libs/class_url.php');
require('models/libs/class_funciones.php');

######CLASES DISPLAY######
require('models/display/class_replace.php');
require('models/display/class_display.php');
require('models/display/class_display_categorias.php');
require('models/display/class_display_foro.php');
require('models/display/class_display_tema.php');
require('models/display/class_display_index.php');
require('models/display/class_display_user.php');

############################# FIN CLASES #############################

########################### BASE DE DATOS ###########################
#CONEXION
const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASS = 'poncho';

########################## FIN BASE DE DATOS

########################## CONSTANTES GENERALES #####################
##### CANTIDAD MINIMA DE CARACTERES TEMA
const CANTIDAD_TEMA = 9;
##### CANTIDAD MINIMA DEL CONTENIDO DEL TEMA
const CANTIDAD_CONTENIDO = 270;
##### CANTIDAD MINIMA DEL USUARIO
const CANTIDAD_USER = 6;
##### CANTIDAD MINIMA DEL PASSWORD
const CANTIDAD_PASSWORD = 6;
##### CANTIDAD MINIMA DE CATEGORIA
const CANTIDAD_CATEGORIA = 8;
##### CANTIDAD MINIMA DE LA DESCRIPCION DE LA CATEGORIA
const CANTIDAD_CATEGORIA_DESCRIPCION = 25;
##### CANTIDAD MINIMA DEL FORO
const CANTIDAD_FORO = 8;
##### CANTIDAD MINIMA DE LA DESCRIPCION DEL FORO
const CANTIDAD_FORO_DESCRIPCION = 25;
##### CANTIDAD MINIMA DE LA DESCRIPCION DEL FORO
const CANTIDAD_RESPUESTA = 10;
######################### FIN DE CONSTANTES GENERALES #######################

########################### OBJETOS ###############################
########################### USUARIOS ###################################
####### USERS ######
$__USER = new user_model();
$__USER->ALL_USERS();
$__USER->set_data();
### SE CREO $_SESSION['users'] CONTIENE TODOS LOS USERS
##### SE CREO $_SESSION['cantidad_user'] CONTIENE LA CANTIDAD DE USERS
########################### FIN USUARIOS ##################################

########################### CATEGORIAS ##################################
##########CATEGORY #########
$__CATEGORIA = new categoria_model();
$__CATEGORIA->ALL_CATEGORY();
$__CATEGORIA->set_data();
##### SE CREO $_SESSION['category'] CONTIENE TODAS LAS CATEGORIAS
##### SE CREO $_SESSION['cantidad_categoria'] CONTIENE LA CANTIDAD DE CATEGORIAS
######################### FIN CATEGORIAS ###############################

########################### FOROS ########################
#############FORO########################
$__FORO = new foro_model();
$__FORO->ALL_FORO();
$__FORO->set_data();
##### SE CREO $_SESSION['foro'] CONTIENE TODOS LOS FOROS
##### SE CREO $_SESSION['cantidad_foro'] CONTIENE LA CANTIDAD DE FOROS
############### FIN FOROS #####################

#GENERAL
#####FUNCIONES GENERALES COMPROBACION Y DEMAS
$__ = new funciones_model();
##### Con prueba la ultima conexion del usuario
$__->__ULT_CON();
############# OBJETOS DE VISTAS #################
$__TEMPLATE = new class_display();
##### INFORMACION BASE
const BASE_URL = 'http://localhost/Foro/';
const TITLE = 'Super Foro';

##### CABECERA
const HEAD = 'view/includes/general/head.html';
$DICCIONARIO_CABECERA =  array(
  'TITLE' => TITLE,
  'BASE_URL' => BASE_URL);

####### MIJAS
const HOME_MIJA = 'home_mija.png';
const TEMA_MIJA = 'tema_mija.png';
const TEMA_NUEVO_MIJA = 'tema_nuevo_mija.png';
$DICCIONARIO_MIJA = array(
  'Inicio' => array(
    0 => HOME_MIJA,
    1 => '?view=index'
   )
 );

######### NAV
const NAV_SIN_SESION = 'view/includes/general/nav/sin_sesion.html';
const NAV_CON_SESION = 'view/includes/general/nav/con_sesion.html';
const NAV_ADMINISTRADOR = 'view/includes/general/nav/nav_administrador.html';
const NAV_ADMIN_TODOS_CAT = 'view/includes/general/nav/admin_foro/categorias/nav_admin_todos_cat.html';
const NAV_ADMIN_AGREGAR_CAT = 'view/includes/general/nav/admin_foro/categorias/nav_admin_agregar_cat.html';
const NAV_ADMIN_TODOS_FORO = 'view/includes/general/nav/admin_foro/foro/nav_admin_todos_foro.html';
const NAV_ADMIN_AGREGAR_FORO = 'view/includes/general/nav/admin_foro/foro/nav_admin_agregar_foro.html';
$DICCIONARIO_NAV = array(
  'MODAL_LOGIN' => 'view/includes/modal/login.html',
  'MODAL_REGISTRO' => 'view/includes/modal/registro.html',
  'MODAL_RECUPERAR_PASSWORD' => 'view/includes/modal/recuperar_password.html',
  'MIJAS' => $__->dinamic_mijas($DICCIONARIO_MIJA),
  'ID_USER' => '',
  'NOMBRE_USER' => '',
  'BOTON' => '');

#### CONTENIDO DE LA PAGINA
const CONTENIDO = 'view/includes/general/body.html';
const AGREGAR_CATEGORIA = 'view/includes/contenido/categoria/agregar_categoria.html';
const EDITAR_CATEGORIA = 'view/includes/contenido/categoria/editar_categoria.html';
const AGREGAR_FORO = 'view/includes/contenido/foro/agregar_foro.html';
const EDITAR_FORO = 'view/includes/contenido/foro/editar_foro.html';
const AGREGAR_TEMA = 'view/includes/contenido/tema/agregar_tema.html';
const MOSTRAR_TEMA = 'view/includes/contenido/tema/mostrar_tema.html';
const EDITAR_TEMA = 'view/includes/contenido/tema/editar_tema.html';
const RESPONDER_TEMA = 'view/includes/contenido/tema/responder_tema.html';
const EDITAR_RESPUESTA_TEMA = 'view/includes/contenido/tema/editar_respuesta_tema.html';
const PERFIL = 'view/includes/contenido/usuario/perfil.html';
const EDITAR_PERFIL = 'view/includes/contenido/usuario/editar_perfil.html';
$DICCIONARIO_CONTENIDO = array(
  'CONTENIDO' => '');

#### PIE DE LA PAGINA
const FOOTER = 'view/includes/general/footer.html';
############### FIN GENERAL ######################

##################### ALERTAS #####################
##### Alertas pequeñas (para modales, formularios, etc)
const ERROR = 'view/includes/alertas/error.html';
const ERROR_A_MODAL = 'view/includes/alertas/error_a_modal.html';
const SUCCESS = 'view/includes/alertas/success.html';

##### Alertas para paginas completas
const ERROR_PAGINA_LINEAL = 'view/includes/contenido/error_lineal.html';
const ERROR_PAGINA_LLENO = 'view/includes/contenido/error_lleno.html';
const SUCCESS_PAGINA_LINEAL = 'view/includes/contenido/success_lineal.html';
$DICCIONARIO_ALERTA = array(
  'TITULO' => '', // Alerta y Error de pagina
  'SUBTITULO' => '', // Error de pagina
  'CONTENIDO' => '', // Alerta y Error de pagina
  'MODAL' => '', // Error de pagina
  'CONTENIDO_MODAL' => ''); // Error de pagina
#################### FIN ALERTAS ####################

###################### EMAIL #######################
const EMAIL_ACTIVACION = 'view/includes/email/activacion.html';
const EMAIL_RECUPERAR_PASS = 'view/includes/email/recuperar_password.html';
const EMAIL_RECUPERAR_PASS_NEWPASS = 'view/includes/email/recuperar_password_newpass.html';
$DICCIONARO_EMAIL = array(
  'TITULO' => TITLE,
  'USER' => '',
  'FECHA' => '',
  'LINK' => '');
##################### FIN EMAIL ######################
########################### TERMINA VISTAS ################################
 ?>
