<?php
#### Se enfocara en remplazar los nombres solamente #####

class class_replace {

      #######################METODOS PRINCIPALES ##########################
      function get_template($directorio = ""){
        if(is_string($directorio)){
          if (file_exists($directorio)) {
            $html = file_get_contents($directorio);
            return $html;
          }else{
            return $directorio;
          }
        }
      }
      function replace ($html,$diccionario){
        if(is_array($diccionario)){
          foreach ($diccionario as $key => $value) {
            if(!is_array($value)){
              $html = str_replace('{'.$key.'}',$value,$html);
            }
          }
        }
        return $html;
      }
      function replace_dicc($diccionario){
        if(is_array($diccionario)){
          foreach ($diccionario as $key => $value) {
            if(!is_array($value)){
              $html = $this->get_template($value);
              $diccionario[$key] = $html;
            }
          }
        }
        return $diccionario;
      }
      ##################### FIN #########################

      ########################## CONSTRUCTOR Y DESTRUCTOR ################################
      function __destruct(){
          unset($this);
      }
      ##################### FIN #########################
  }
 ?>
