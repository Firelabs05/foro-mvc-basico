<?php
/*
* Muestra el contenido de las categorias
* Crea dinamicamente los div de las categorias
*/
class class_display_categorias {
      protected $categorias;
      protected $cantidad_categorias;

      #######################METODOS PRINCIPALES ##########################
      /*
      * Devuelve el div cabecera de las categorias
      */
      public function dinamic_div_head(){
        $html = '{CONTENIDO}
                  <div class="table-dark col-lg-12 row">
                      <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Categoria:</h6>
                      </div>
                      <div class = "col-lg-8">
                        <h6 class ="top_mar_15">Descripcion:</h6>
                      </div>
                      <div class = "col-lg-2">
                        <h6 class ="top_mar_15">Acciones:</h6>
                      </div>
                  </div>';
        return $html;
      }
      /*
      * Se le pasa un arreglo que contenga la informacion de la categoria
      * devuelve el div body
      */
      public function dinamic_div_body($categoria = []){
          $html = '';
          foreach ($categoria as $key_categoria => $value_categoria) {
              $html .='<div class ="border_bot table-secondary row col-lg-12">';
              foreach ($value_categoria as $key_categoria2 => $value_categoria2) {
                switch ($key_categoria2) {
                  case 'id_categoria':
                    break;
                  case 'descripcion':
                      $html.='<div class = "top_bot_mar_15 col-lg-8">'.$value_categoria2.'</div>';
                    break;
                  default:
                      $html.='<div class = "top_bot_mar_15 col-lg-2">'.$value_categoria2.'</div>';
                    break;
                }
              }
              $html .= '<div class ="top_bot_mar_15 col-lg-2">
                          <div class="btn-group" role="group">
                            <button id="" type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Acciones
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop3">
                              <a class="dropdown-item" href="?view=categoria&categoria=pagina_editar&id_categoria='.$value_categoria['id_categoria'].'">
                                Editar
                              </a>
                              <button type="button" class="dropdown-item" name ="eliminar" value="'.$value_categoria['id_categoria'].'">
                                Eliminar
                              </button>
                            </div>
                          </div>
                        </div>';
              $html .='</div>';
          }
          $html .= '<script src ="view/js/categoria/borrar_categoria.js">
                    </script>';
          return $html;
      }
      /*
      * Es el constructor del div, se encarga de formar el CONTENIDO
      */
      public function dinamic_categoria(){
        if ($this->cantidad_categorias != $_SESSION['cantidad_categoria']){
          self::__construct();
          return $this->dinamic_categoria();
        } else {
          $html = "";
          if($this->cantidad_categorias > 0){
            $html = $this->dinamic_div_head();
            $html .= $this->dinamic_div_body($this->categorias);
          }else {
            $html = $this->dinamic_div_empty();
          }
          return $html;
        }
      }
      /*
      * Devuelve el div central con diciendo que esta vacio
      */
      public function dinamic_div_empty(){
          $html ='<div class = "row container">
                  <div class ="alert alert-secondary col-lg-12">
                    <h3 style="text-align: center;">
                      Esta todo vacio, tan vacio como tu cuenta bancaria
                    </h3>
                  </div>
                  </div>';
          return $html;
      }
      ##################### FIN #########################

      ########################## CONSTRUCTOR Y DESTRUCTOR ################################
      function __construct(){
        if(isset($_SESSION['category']) && !empty($_SESSION['category'])
          && isset($_SESSION['cantidad_categoria']) && !empty($_SESSION['cantidad_categoria'])){
            $this->categorias = $_SESSION['category'];
            $this->cantidad_categorias = $_SESSION['cantidad_categoria'];
        }
      }
      function __destruct(){
          unset($this);
      }
      ##################### FIN #########################
  }
 ?>
