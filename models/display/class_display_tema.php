<?php
/**
 *  Se encarga de mostrar el contenido de los temas
 */
class class_display_tema{
      ####################### METODOS PRINCIPALES ##########################
      /*
      * Devuelve el div central con diciendo que esta vacio
      */
      public function dinamic_div_empty(){
        $html ='<div class = "row container">
                  <div class ="alert alert-secondary col-lg-12">
                    <h3 style="text-align: center;">
                      Esta todo vacio, tan vacio como tu cuenta bancaria
                    </h3>
                  </div>
                </div>';
        return $html;
      }
      ####################### METODOS AGREGAR TEMA #######################
      /*
      * Crea el checkbox para saber si el tema sera de anuncio o no
      */
      public function dinamic_check_anuncio($check = false){
        $html = '<div class="form-group">
                    <div class="custom-control custom-checkbox">';
        if ($check) {
          $html .= '<input class="custom-control-input" id = "anuncio" name = "anuncio" type="checkbox" checked>';
        } else {
          $html .= '<input class="custom-control-input" id = "anuncio" name = "anuncio" type="checkbox">';
        }
        $html .= '<label class="custom-control-label" for="anuncio">
                    Anuncio
                  </label>
                </div>
              </div>';


        return $html;
      }
      ####################################################################
      ###################### METODOS DE FOROS TEMAS #################
      /*
      * Crea el boton de nuevo tema
      */
      public function dinamic_boton($value = "", $text = ""){
        $boton = '<div>
                    <a href="'.$value.'" class="btn btn-info btn-lg">
                      '.$text.'
                    </a>
                  </div>';
        return $boton;
      }
      public function dinamic_boton_eliminar($id_foro = "", $id_tema = ""){
        $boton = '<div>
                    <button type="input" class="btn btn-info btn-lg" id = "eliminar" value = "'.$id_tema.'">
                      Eliminar
                    </button>
                    <input type="hidden" id = "id_foro" value = "'.$id_foro.'">
                  </div>';
        return $boton;
      }
      /*
      * Crea el contenido dinamico del tema
      */
      public function dinamic_temas($id_foro){
        // Se crea el objeto que obtendra toda la informacion de los temas
        $tema = new tema_model();
        $tema->set_id_foro($id_foro);
        // 0 para anuncios y 1 para no anuncios
        $tema->set_tipo(1);
        $tema->get_tema();
        $data = $tema->get_data();
        // Se vacia el data
        $tema->set_data();
        $cantidad = count($data);
        // Se empieza a crear los temas con anuncios
        $html = "";
        if($cantidad > 0){
          $html .= $this->dinamic_head_anuncios();
          $html .= $this->dinamic_tema_body($data,$id_foro);
        }else{
          $html .= $this->dinamic_head_anuncios();
          $html .= $this->dinamic_div_empty();
        }
        // Realiza la misma accion solo que con los no anuncios
        $tema->set_tipo(0);
        $tema->get_tema();
        $data = $tema->get_data();
        $cantidad = count($data);
        $tema->__destruct();
        if($cantidad > 0){
          $html .= $this->dinamic_head_no_anuncios(
                                $_SESSION['foro'][$id_foro]['nombre'],
                                $_SESSION['foro'][$id_foro]['descripcion']);
          $html .= $this->dinamic_tema_body($data,$id_foro);
        }else{
          $html .= $this->dinamic_head_no_anuncios(
                                $_SESSION['foro'][$id_foro]['nombre'],
                                $_SESSION['foro'][$id_foro]['descripcion']);
          $html .= $this->dinamic_div_empty();
        }
        return $html;
      }
      /*
      * Se encarga de crear las cabeceras del contenido
      */
      public function dinamic_head_anuncios(){
        $html = '<div class="table-dark col-lg-12 row">
                  <div class = "col-lg-12 text_center">
                    <h5 class = "titulo top_bot_mar_15">Anuncios</h5>
                  </div>
                </div>';
        $html .= '<div class="table-dark col-lg-12 row">
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Estado:</h6>
                    </div>
                    <div class = "col-lg-3">
                      <h6 class ="top_mar_15">Tema:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Visitas:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Respuestas:</h6>
                    </div>
                    <div class = "col-lg-3">
                      <h6 class ="top_mar_15">Ultimo mensaje:</h6>
                    </div>
                  </div>';
        return $html;
      }
      public function dinamic_head_no_anuncios($nombre,$descripcion){
        $html = '<div class="table-dark col-lg-12 row">
                  <div class = "col-lg-3">
                    <h6 class ="top_mar_15">Foro:</h6>
                    <h5 class = "titulo"> '.$nombre.'</h5>
                  </div>
                  <div class = "col-lg-9">
                    <h6 class ="top_mar_15">Descripcion:</h6>
                      <p class ="top_mar_15">'.$descripcion.'</p>
                  </div>
                </div>';
        $html .= '<div class="table-dark col-lg-12 row">
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Estado:</h6>
                    </div>
                    <div class = "col-lg-3">
                      <h6 class ="top_mar_15">Tema:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Visitas:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Respuestas:</h6>
                    </div>
                    <div class = "col-lg-3">
                      <h6 class ="top_mar_15">Ultimo mensaje:</h6>
                    </div>
                  </div>';
        return $html;
      }
      /*
      * Se encarga del cuerpo
      */
      public function dinamic_tema_body($tema,$id_foro){
        $url = new class_url_amigable();
        $html ='<div class ="table-secondary col-lg-12 row">';
        foreach ($tema as $key => $value) {
          foreach ($value as $key2 => $value2) {
            switch ($key2) {
              case 'id_tema':
              case 'id_user_u':
              case 'id_user_d':
              break;
              case 'estado':
                if($value2 == 1){
                  $html.='<div class = "top_mar_15 col-lg-2">
                            <img src="view/img/tema_abierto.png" alt="">
                          </div>';
                }else{
                  $html.='<div class = "top_mar_15 col-lg-2">
                            <img src="view/img/foro_cerrado.png" alt="">
                          </div>';
                }
              break;
              case 'visitas':
                $html.='<div class = "top_mar_15 col-lg-2">
                          '.$value2.'
                        </div>';
              break;
              case 'respuestas':
                $html.='<div class = "top_mar_15 col-lg-2">
                          '.$value2.'
                        </div>';
              break;
              case 'nombre':
                $url->set_id_1($value['id_tema']);
                $url->set_titulo($value['nombre']);
                $url->set_id_2($id_foro);
                $html.='<div class = "top_mar_15 col-lg-3">
                          <a class="text-info sin_subrayado" href="tema/'.$url->url_amigable().'">
                            '.$value2.'
                          </a>
                          <br>Por:
                          <a class="text-info sin_subrayado" href="?view=perfil&id_user='.$value['id_user_d'].'">
                            '.$_SESSION['users'][$value['id_user_d']]['user'].'
                          </a>
                        </div>';
              break;
              case 'fecha_ultimo':
                $html.='<div class = "top_mar_15 col-lg-3">
                          por:
                          <a class="text-info sin_subrayado" href="?view=perfil&id_user='.$value['id_user_u'].'">
                            '.$_SESSION['users'][$value['id_user_u']]['user'].'
                          </a>
                          <br>Fecha: '.$value2.'
                        </div>';
              break;
              default:
                $html.='<div class = "top_mar_15 col-lg-2">
                          '.$value2.'
                        </div>';
              break;
            }
          }
        }
        $html.='</div>';
        return $html;
      }
      ################# MOSTRAR RESPUESTAS #####################################
      public function dinamic_respuestas($id_tema = "", $id_foro = ""){
        $respuestas = new responder_model();
        $respuestas->set_id_tema($id_tema);
        $respuestas->get();
        $data = $respuestas->get_data();
        $html = "";
        foreach ($data as $key => $value) {
          // Funcion que creara la respuesta
          $html .= $this->div_respuesta($_SESSION['users'][$value['id_user']],$value,$id_foro);
        }
        return $html;
      }
      public function div_respuesta($user = [],$respuesta = [], $id_foro = []){
        $__ = new funciones_model();
        if($__->__USER_ACT($user['ultima_con'])){
          $imagen_online = 'view/img/online.png';
        }else {
          $imagen_online = 'view/img/offline.png';
        }
        $html = '<div class="row">
          <div class="card col-lg-2 bot_padd_15">
            <div class="card">
              <img style="height: 100%; width: 100%; display: block;"
              src="view/img/user_perfil/'.$user['img'].'" alt="Card image">
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Nombre: '.$user['user'].'</li>
              <li class="list-group-item">Online:
                 <img style="height: 24px; width: 24px; " src="'.$imagen_online.'" alt="Card image">
               </li>
              <li class="list-group-item">Tipo: '.$__->__USER_TIP($user['permiso']).'</li>
              <li class="list-group-item">Rango: '.$__->__USER_RANG($user['rango']).'</li>
            </ul>
          </div>
          <div class="row col-lg-10">
            <div class="card col-lg-12">
              <div class="card-body">';
              if ($_SESSION['foro'][$id_foro]['estado'] == 1) {
                if (isset($_SESSION['id_user'])
                && ($_SESSION['users'][$_SESSION['id_user']]['permiso'] > 0
                || $_SESSION['id_user'] == $respuesta['id_user'])) {
                  $html .= '<div>
                              <a href="?view=tema&mode=editar_respuesta&id_foro='.$id_foro.'&id_tema='.$respuesta['id_tema'].'&id_respuesta='.$respuesta['id_respuesta'].'" class="btn btn-info btn-lg">
                                Editar
                              </a>
                              <button type="input" class="btn btn-info btn-lg" name = "eliminar_respuesta" value = "'.$respuesta['id_respuesta'].'">
                                Eliminar
                              </button>
                            </div>';
                }
              }
              $html .= '<p class="card-text">'.$respuesta['contenido'].'</p>
              </div>
              <div class="card-footer">
                '.$__->BBcode($user['firma']).'
              </div>
            </div>
          </div>
        </div>';
        return $html;
      }
      ##########################################################################
      ###############################################################

      ########################## CONSTRUCTOR Y DESTRUCTOR ################################
      function __construct(){

      }
      function __destruct(){
        unset($this);
      }
  ##################### FIN #########################
}

?>
