<?php
/*
* Muestra el contenido de los foros
* Crea dinamicamente los div de los foros
*/
class class_display_foro {
      protected $foro;
      protected $cantidad_foro;
      protected $categorias;
      protected $cantidad_categorias;
      protected $tema_anuncio;
      protected $cantidad_anuncio;
      protected $tema_no_anuncio;
      protected $cantidad_no_anuncio;

      ####################### METODOS PRINCIPALES ##########################
      /*
      * Devuelve el div cabecera de los foros
      */
      public function dinamic_div_head(){
        $html = '{CONTENIDO}
                  <div class="table-dark col-lg-12 row">
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Foro:</h6>
                    </div>
                    <div class = "col-lg-3">
                      <h6 class ="top_mar_15">Descripcion:</h6>
                    </div>
                    <div class = "col-lg-1">
                      <h6 class ="top_mar_15">Cantidad msj:</h6>
                    </div>
                    <div class = "col-lg-1">
                      <h6 class ="top_mar_15">Cantidad temas:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Categoria:</h6>
                    </div>
                    <div class = "col-lg-1">
                      <h6 class ="top_mar_15">Estado:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Acciones:</h6>
                    </div>
                  </div>';
        return $html;
      }
      /*
      * Se le pasa un arreglo que contenga la informacion de el foro
      * devuelve el div body
      */
      public function dinamic_div_body($foro = []){
          $html = '';
          foreach ($foro as $key_foro => $value_foro) {
              $html .='<div class ="border_bot table-secondary row col-lg-12">';
              foreach ($value_foro as $key_foro2 => $value_foro2) {
                switch ($key_foro2) {
                  case 'id_foro':
                    break;
                  case 'nombre':
                  case 'categoria':
                    $html .='<div class = "top_bot_mar_15 col-lg-2">'.$value_foro2.'</div>';
                    break;
                  case 'descripcion':
                    $html .='<div class = "top_bot_mar_15 col-lg-3">'.$value_foro2.'</div>';
                    break;
                  default:
                    $html .='<div class = "top_bot_mar_15 col-lg-1">'.$value_foro2.'</div>';
                    break;
                }
              }
              $html .= '<div class ="top_bot_mar_15 col-lg-2">
                          <div class="btn-group" role="group">
                            <button id="" type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Acciones
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop3">
                              <a class="dropdown-item" href="?view=foro&foro=pagina_editar&id_foro='.$value_foro['id_foro'].'">
                                Editar
                              </a>
                              <button type="button" class="dropdown-item" name ="eliminar" value="'.$value_foro['id_foro'].'">
                                Eliminar
                              </button>
                            </div>
                          </div>
                        </div>';
              $html .='</div>';
          }
          $html .= '<script src ="view/js/foro/borrar_foro.js">
                    </script>';
          return $html;
      }
      /*
      * Es el constructor del div, se encarga de formar el CONTENIDO
      */
      public function dinamic_foro(){
        if ($this->cantidad_foro != $_SESSION['cantidad_foro']){
          self::__construct();
          return $this->dinamic_foro();
        } else {
          $html = "";
          if($this->cantidad_foro > 0){
            $html = $this->dinamic_div_head();
            $html .= $this->dinamic_div_body($this->foro);
          }else {
            $html = $this->dinamic_div_empty();
          }
          return $html;
        }
      }
      /*
      * Devuelve el div central con diciendo que esta vacio
      */
      public function dinamic_div_empty(){
          $html ='<div class = "row container">
                  <div class ="alert alert-secondary col-lg-12">
                    <h3 style="text-align: center;">
                      Esta todo vacio, tan vacio como tu cuenta bancaria
                    </h3>
                  </div>
                  </div>';
          return $html;
      }
      /*
      * Devuelve el estado del foro
      */
      public function dinamic_estado($chek = NULL){
        if ($chek == 1) {
          $activo = '<input type="radio" class="form-check-input" name="estado_foro" value="1" checked="">';
          $cerrado = '<input type="radio" class="form-check-input" name="estado_foro" value="0">';
        } else {
          $activo = '<input type="radio" class="form-check-input" name="estado_foro" value="1" >';
          $cerrado = '<input type="radio" class="form-check-input" name="estado_foro" value="0" checked="">';
        }
        $estado ='<div class="form-check">
                  <label class="form-check-label">
                  '.$activo.'
                  Activo
                </label>
              </div>
              <div class="form-check">
                <label class="form-check-label">
                  '.$cerrado.'
                  Cerrado
                </label>
              </div>';
        return $estado;
      }
      /*
      * Devuelve el los option con las categorias
      */
      public function dinamic_option($chek = NULL){
        self::__construct_cat();
        $option ='';
        if ($this->cantidad_categorias > 0) {
          foreach ($this->categorias as $key => $value) {
              if($chek == $value['id_categoria']){
                $option.='<option value = "'.$value['id_categoria'].'" selected>'.$value['nombre'].'</option>';
              }else{
                $option.='<option value = "'.$value['id_categoria'].'">'.$value['nombre'].'</option>';
              }
          }
        } else {
          $option .= '<option value = "0">No hay foros</option>';
        }
        return $option;
      }
      ################# FIN #########################

      ########################## CONSTRUCTOR Y DESTRUCTOR ################################
      function __construct(){
        $data = new foro_model();
        $data->get_foros();
        $this->foro = $data->get_data();
        $this->cantidad_foro = count($data->get_data());
        $data->__destruct();
      }
      function __construct_cat(){
        if(isset($_SESSION['category']) && !empty($_SESSION['category'])
          && isset($_SESSION['cantidad_categoria']) && !empty($_SESSION['cantidad_categoria'])){
            $this->categorias = $_SESSION['category'];
            $this->cantidad_categorias = $_SESSION['cantidad_categoria'];
        }
      }
      function __destruct(){
          unset($this);
      }
      ##################### FIN #########################
  }
 ?>
