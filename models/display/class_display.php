<?php
/*
* Se encarga de crear la pagina con sus metodos, solo son remplazar con las librerias
*/
class class_display extends class_replace {

    #######################METODOS PRINCIPALES ##########################
    // METODOS CREADORES DE LA PAGINA
    // Crea el nav de forma automatica
    public function display_nav($DICCIONARIO_NAV = []){
      $directorio = "";
      if(isset($_SESSION['id_user'])
        && array_key_exists($_SESSION['id_user'],$_SESSION['users'])){
        // Se le pasa el id y el nombre del user
        $DICCIONARIO_NAV['ID_USER'] = $_SESSION['id_user'];
        $DICCIONARIO_NAV['NOMBRE_USER'] = $_SESSION['users'][$_SESSION['id_user']]['user'];
        if ($_SESSION['users'][$_SESSION['id_user']]['permiso'] == 2) {
          // Administrador
          $directorio = NAV_ADMINISTRADOR;
        } else {
          // Usuario normal
          $directorio = NAV_CON_SESION;
        }
      }else {
        // NO usuario
        $directorio = NAV_SIN_SESION;
      }
      $html = $this->get_template($directorio);
      $DICCIONARIO_NAV = $this->replace_dicc($DICCIONARIO_NAV);
      $html = $this->replace($html,$DICCIONARIO_NAV);
      return $html;
    }
    // Crea el nav de forma manual
    public function display_nav_manual($directorio = "", $DICCIONARIO_NAV = []){
      $html = $this->get_template($directorio);
      $DICCIONARIO_NAV = $this->replace_dicc($DICCIONARIO_NAV);
      $html = $this->replace($html,$DICCIONARIO_NAV);
      return $html;
    }
    // Añade el footer
    public function display_footer($directorio = ""){
      $html = $this->get_template($directorio);
      return $html;
    }
    // Añade el contenido central
    public function display_contenido($directorio = "", $contenido = []){
      $html = $this->get_template($directorio);
      $html = $this->replace($html,$contenido);
      return $html;
    }
    // FIN METODOS CREADORES DE LA PAGINAS
    ##################### FIN ########################


    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################
}
?>
