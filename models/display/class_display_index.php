<?php
/*
* Muestra el contenido del index
* Crea dinamicamente los div de las categorias y de los foros que tienen
*/
class class_display_index{
      protected $categorias;
      protected $cantidad_categorias;

      #######################METODOS PRINCIPALES ##########################
      /*
      * Se le pasa un arreglo que contenga la informacion de la categoria
      * devuelve el div cabecera con nombre y descripcion
      */
      public function dinamic_div_categoria($categoria = []){
        $html = '<div class="table-dark col-lg-12 row border_blanco">
                      <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Categoria:</h6>
                      <h5 class = "titulo"> '.$categoria['nombre'].'</h5>
                      </div>
                      <div class = "col-lg-10">
                        <h6 class ="top_mar_15">Descripcion:</h6>
                        <p class ="top_mar_15">'.$categoria['descripcion'].'</p>
                      </div>
                  </div>';
        $html .= '<div class="table-dark col-lg-12 row">
                    <div class = "col-lg-1">
                      <h6 class ="top_mar_15">Estado:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Foro:</h6>
                    </div>
                    <div class = "col-lg-5">
                      <h6 class ="top_mar_15">Descripcion:</h6>
                    </div>
                    <div class = "col-lg-1">
                      <h6 class ="top_mar_15">Temas:</h6>
                    </div>
                    <div class = "col-lg-1">
                      <h6 class ="top_mar_15">Mensajes:</h6>
                    </div>
                    <div class = "col-lg-2">
                      <h6 class ="top_mar_15">Ultimo tema:</h6>
                    </div>
                  </div>';
        return $html;
      }
      /*
      * Se le pasa un arreglo que contenga la informacion del foro
      * devuelve el div central con todos los foros
      */
      public function dinamic_div_body($foro = []){
        $url = new class_url_amigable();
        $html = "";
        foreach($foro as $key_foro => $value_foro){
          $html .='<div class ="table-secondary row col-lg-12">';
          foreach($value_foro as $key_foro_2 => $value_foro_2){
            switch ($key_foro_2) {
              case 'id_foro':
              case 'id_ultimo_tema':
              break;
              case 'estado':
                $html .= '<div class = "top_bot_mar_15 col-lg-1 ';
                if($value_foro_2 == 1){
                  $html.= 'foro_abierto">';
                }else{
                  $html.='foro_cerrado">';
                }
                $html .= '</div>';
              break;
              case 'cantidad_msj':
                $html .='<div class = "top_bot_mar_15 col-lg-1">
                          '.$value_foro_2.'
                        </div>';
              break;
              case 'cantidad_tem':
                $html .='<div class = "top_bot_mar_15 col-lg-1">
                          '.$value_foro_2.'
                        </div>';
              break;
              case 'nombre':
                $url->set_id_1($value_foro['id_foro']);
                $url->set_titulo($value_foro['nombre']);
                $html.='<div class = "top_bot_mar_15 col-lg-2">
                          <a class="text-info sin_subrayado" href="foro/'.$url->url_amigable().'">
                            '.$value_foro_2.'
                          </a>
                        </div>';
                $url->reset();
              break;
              case 'nom_ultimo_tema':
              $html .= '<div class = "top_bot_mar_15 col-lg-2">';
                if($value_foro_2 == ''){
                  $html.='No hay temas';
                }else{
                  $url->set_id_1($value_foro['id_ultimo_tema']);
                  $url->set_titulo($value_foro['nom_ultimo_tema']);
                  $url->set_id_2($value_foro['id_foro']);
                  $html.='<a class="text-info sin_subrayado" href="tema/'.$url->url_amigable().'">
                              '.$value_foro_2.'
                          </a>';
                  $url->reset();
                }
                $html .= '</div>';
              break;
              default:
                $html.='<div class = "top_bot_mar_15 col-lg-5">
                        '.$value_foro_2.'
                        </div>';
              break;
            }
          }
          $html.='</div>';
        }
        $url->__destruct();
        return $html;
      }
      /*
      * Es el constructor del div, se encarga de formar el CONTENIDO
      */
      public function dinamic_index(){
        $html = "";
        if($this->cantidad_categorias > 0){
          $foro = new foro_model();
          foreach($this->categorias as $key_categoria => $value_categoria) {
            $html .= '<div class="container row">';
            $html .= $this->dinamic_div_categoria($value_categoria);
            $foro->set_id_categoria($key_categoria);
            $foro->get_foro_id_categoria();
            $data = $foro->get_data();
            if(count($data) > 0){
              $html .= $this->dinamic_div_body($data);
              $foro->set_data();
            } else {
              $html .= $this->dinamic_div_empty();
            }
            $html .= '</div>';
          }
          $foro->__destruct();
        }else{
          $html .= $this->dinamic_div_empty();
        }
        return $html;
      }
      /*
      * Devuelve el div central con diciendo que esta vacio
      */
      public function dinamic_div_empty(){
          $html ='<div class = "row container">
                  <div class ="alert alert-secondary col-lg-12">
                    <h3 style="text-align: center;">
                      Esta todo vacio, tan vacio como tu cuenta bancaria
                    </h3>
                  </div>
                  </div>';
          return $html;
      }
      ##################### FIN #########################

      ########################## CONSTRUCTOR Y DESTRUCTOR ################################
      function __construct(){
        if(isset($_SESSION['category']) && !empty($_SESSION['category'])
          && isset($_SESSION['cantidad_categoria']) && !empty($_SESSION['cantidad_categoria'])){
            $this->categorias = $_SESSION['category'];
            $this->cantidad_categorias = $_SESSION['cantidad_categoria'];
          }
      }
      function __destruct(){
          unset($this);
      }
      ##################### FIN #########################
  }
 ?>
