<?php
class funciones_model {

    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################

    #######################METODOS PRINCIPALES ##########################
    ######################################################
    ##### Se encarga de construir las mijas
    public function dinamic_mijas($__s = []){
      $html = '<ol class="breadcrumb">';
      foreach ($__s as $key => $value) {
        if($value === end($__s)){
          $html .= '<li class="breadcrumb-item active">
                      <img src="view/img/mijas/'.$value[0].'"/>
                      '.$key.'
                    </li>';
        } else {
          $html .= '<li class="breadcrumb-item">
                      <img src="view/img/mijas/'.$value[0].'"/>
                      <a href= "'.$value[1].'">'.$key.'</a>
                    </li>';
        }

      }
      $html .= '</ol>';
      return $html;
    }
    ################################################################
    ######## Devuelve el tipo de usuario en un string
    public function __USER_TIP($value = 0){
      switch ($value) {
        case '1':
          $tipo = 'Moderador';
        break;
        case '2':
          $tipo = 'Administrador';
        break;
        default:
          $tipo = 'Usuario';
        break;
      }
      return $tipo;
    }
    ################################################################
    ######## Devuelve el rango de usuario en un string
    public function __USER_RANG($value = 0){
      switch ($value) {
        case '1':
          $tipo = 'Super novato';
        break;
        case '2':
          $tipo = 'Novato Experto';
        break;
        default:
          $tipo = 'Novato';
        break;
      }
      return $tipo;
    }
    ################################################################
    ######### Devuelve el texto pero ya con BBcode
    public function BBCode($string) {
        $BBcode = array(
            '/\[i\](.*?)\[\/i\]/is',
            '/\[b\](.*?)\[\/b\]/is',
            '/\[u\](.*?)\[\/u\]/is',
            '/\[s\](.*?)\[\/s\]/is',
            '/\[img\](.*?)\[\/img\]/is',
            '/\[center\](.*?)\[\/center\]/is',
            '/\[h1\](.*?)\[\/h1\]/is',
            '/\[h2\](.*?)\[\/h2\]/is',
            '/\[h3\](.*?)\[\/h3\]/is',
            '/\[h4\](.*?)\[\/h4\]/is',
            '/\[h5\](.*?)\[\/h5\]/is',
            '/\[h6\](.*?)\[\/h6\]/is',
            '/\[quote\](.*?)\[\/quote\]/is',
            '/\[url=(.*?)\](.*?)\[\/url\]/is',
            '/\[bgcolor=(.*?)\](.*?)\[\/bgcolor\]/is',
            '/\[color=(.*?)\](.*?)\[\/color\]/is',
            '/\[bgimage=(.*?)\](.*?)\[\/bgimage\]/is',
            '/\[size=(.*?)\](.*?)\[\/size\]/is',
            '/\[font=(.*?)\](.*?)\[\/font\]/is'
        );
        $HTML = array(
            '<i>$1</i>',
            '<b>$1</b>',
            '<u>$1</u>',
            '<s>$1</s>',
            '<img src="$1" />',
            '<center>$1</center>',
            '<h1>$1</h1>',
            '<h2>$1</h2>',
            '<h3>$1</h3>',
            '<h4>$1</h4>',
            '<h5>$1</h5>',
            '<h6>$1</h6>',
            '<blockquote style="background:#f1f5f7;color:#404040;padding:4px;border-radius:4px;">$1</blockquote>',
            '<a href="$1" target="_blank">$2</a>',
            '<div style="background: $1;">$2</div>',
            '<span style="color: $1;">$2</span>',
            '<div style="background: url(\'$1\');">$2</div>',
            '<span style="font-size: $1px">$2</span>',
            '<span style="font-family: $1">$2</span>'
        );
        return nl2br(preg_replace($BBcode,$HTML,$string));
    }
    ###########################################################
    ##### Revisa si ya pasaron 5 minutos desde su ultima conexion y lo revisa
    public function __ULT_CON(){
      if (isset($_SESSION['id_user'])) {
        $time = time();
        if(($_SESSION['users'][$_SESSION['id_user']]['ultima_con']+(60*5)) > $time){
          $user = new user_model;
          $user->set_ultima_con($time);
          $user->set_id_user($_SESSION['id_user']);
          $user->update_ultima_con();
          $user->ALL_USERS(true);
        }
      }
    }
    ######################################################################
    ##### Revisa si el usuario a estado inactivo por mas de 5 minutos
    public function __USER_ACT($ultima_con){
      $time = time();
      if($time-(60*5) > $ultima_con){
        return false;
      }else {
        return true;
      }
    }
    #######################################################################
    ##################### FIN #########################
}
?>
