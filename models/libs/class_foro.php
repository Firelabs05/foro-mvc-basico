<?php
class foro_model extends database {
    private $id_foro;
    private $nombre;
    private $descripcion;
    private $cantidad_msj;
    private $cantidad_tem;
    private $id_categoria;
    private $estado;
    private $id_ultimo_tema;
    private $nom_ultimo_tema;

    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'foro';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################

    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_id_foro($id_foro){
        $this->id_foro = $id_foro;
    }
    public function get_id_foro(){
        return $this->id_foro;
    }
    public function set_nombre($nombre){
      $this->nombre = $nombre;
    }
    public function get_nombre(){
        return $this->nombre;
    }
    public function set_descripcion($descripcion){
      $this->descripcion = $descripcion;
    }
    public function get_descripcion(){
        return $this->descripcion;
    }
    public function set_cantidad_msj($cantidad_msj){
      $this->cantidad_msj = $cantidad_msj;
    }
    public function get_cantidad_msj(){
        return $this->cantidad_msj;
    }
    public function set_cantidad_tem($cantidad_tem){
      $this->cantidad_tem = $cantidad_tem;
    }
    public function get_cantidad_tem(){
        return $this->cantidad_tem;
    }
    public function set_id_categoria($id_categoria = ""){
      if(!empty($id_categoria) && is_numeric($id_categoria)
         && $id_categoria > 0){
        $this->id_categoria = $id_categoria;
      } else {
        $this->id_categoria = 1;
      }
    }
    public function get_id_categoria(){
        return $this->id_categoria;
    }
    public function set_estado($estado){
      $this->estado = $estado;
    }
    public function get_estado(){
        return $this->estado;
    }

    public function set_data($data = []){
      $this->data = $data;
    }
    public function get_data(){
        return $this->data;
    }
    public function get_last_id(){
        return $this->last_id;
    }
    public function set_id_ultimo_tema($id_ultimo_tema){
        $this->id_ultimo_tema = $id_ultimo_tema;
    }
    public function get_id_ultimo_tema(){
        return $this->id_ultimo_tema;
    }
    public function set_nom_ultimo_tema($nom_ultimo_tema){
        $this->nom_ultimo_tema = $nom_ultimo_tema;
    }
    public function get_nom_ultimo_tema(){
        return $this->nom_ultimo_tema;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################
    ######### PAGINA AGREGAR FORO ##########
    #### Agrega el foro a la base de datos
    public function set(){
        $this->sql = "
        INSERT INTO foros SET
        nombre = '".$this->get_nombre()."',
        descripcion = '".$this->get_descripcion()."',
        id_categoria = '".$this->get_id_categoria()."',
        estado = '".$this->get_estado()."'";
        $this->IUDquery();
    }
    ############## FIN ################

    #################### PAGINA TODAS DE FORO #######################
    ##Se encarga de obtener los foros y su informacion
    public function get_foros(){
      $this->sql = "
        SELECT foro.id_foro, foro.nombre, foro.descripcion,
        foro.cantidad_msj, foro.cantidad_tem, cat.nombre AS 'categoria',
        CASE
        WHEN foro.estado = '0' THEN 'Cerrado'
  	    WHEN foro.estado = '1' THEN 'abierto'
        END AS 'estado'
        FROM foros AS foro
        INNER JOIN categorias AS cat
        ON foro.id_categoria = cat.id_categoria
        ORDER BY cat.nombre,foro.nombre";
      $this->Squery();
    }
    ### Elimina el foro
    public function delete(){
      $this->sql = "
        DELETE FROM foros
        WHERE id_foro ='".$this->get_id_foro()."'";
      $this->IUDquery();
    }
    ######################### FIN ################################
    public function edit(){
        $this->sql = "
        SELECT * FROM foros
        WHERE id_foro ='".$this->get_id_foro()."'";
        $this->Squery();
    }
    ################### PAGINA EDITAR FORO ######################
    ### Se encarga de actualizar la informacion del foro
    public function update(){
      $this->sql = "
        UPDATE foros SET
        nombre = '".$this->get_nombre()."',
        descripcion = '".$this->get_descripcion()."',
        id_categoria = '".$this->get_id_categoria()."',
        estado = '".$this->get_estado()."'
        WHERE id_foro ='".$this->get_id_foro()."'";
      $this->IUDquery();
    }
    ########################## FIN ########################

    ##### OBTIENE TODOS LOS FOROS
    public function get(){
      $this->sql = "
        SELECT * FROM foros";
      $this->Squery();
    }
    ####################################
    ######################### ACTUALIZA LOS TEMAS ######################
    ##### Añade el ultimo tema del foro SUMA
    public function update_nuevo_tema(){
      $this->sql = "
        UPDATE foros SET
        cantidad_tem = cantidad_tem + 1,
        id_ultimo_tema = '".$this->get_id_ultimo_tema()."',
        nom_ultimo_tema = '".$this->get_nom_ultimo_tema()."'
        WHERE id_foro ='".$this->get_id_foro()."'";
      $this->IUDquery();
    }
    #######################################
    ################# ACTUALIZA LA CANTIDAD DE TEMAS ###################
    ##### Actualiza la cantidad de temas del foro RESTA
    public function update_eliminar_tema($valor = false){
      $this->sql = "
        UPDATE foros SET
        cantidad_tem = cantidad_tem - 1,
        cantidad_msj = (
          SELECT COUNT(id_respuesta) FROM
          respuestas INNER JOIN temas
          ON temas.id_tema = respuestas.id_tema
          WHERE temas.id_foro = '".$this->get_id_foro()."'),";
      if ($valor) {
        $this->sql .= " id_ultimo_tema = '".$this->get_id_ultimo_tema()."',
         nom_ultimo_tema = '".$this->get_nom_ultimo_tema()."'";
      }else {
        $this->sql .= " nom_ultimo_tema = NULL";
      }
      $this->sql .= " WHERE id_foro ='".$this->get_id_foro()."'";
      $this->IUDquery();
    }
    ##### Actualiza la cantidad de temas del foro RESTA
    public function update_respuesta(){
      $this->sql = "
        UPDATE foros SET
        cantidad_msj = cantidad_msj + 1
        WHERE id_foro ='".$this->get_id_foro()."'";
      $this->IUDquery();
    }
    #######################################
    ##########################################################################
    ########################## ELIMINAR RESPUESTA ###########################
    ##### Actualiza la cantidad respuestas del foro RESTA
    public function update_eliminar_respuesta(){
      $this->sql = "
        UPDATE foros SET
        cantidad_msj = cantidad_msj - 1
        WHERE id_foro ='".$this->get_id_foro()."'";
      $this->IUDquery();
    }
    ######################### PAGINA INDEX ###################################
    ##### Busca el foro por categoria
    public function get_foro_id_categoria(){
      $this->sql = "
        SELECT foro.estado, foro.nombre, foro.descripcion,
        foro.cantidad_tem, foro.cantidad_msj, foro.id_foro,
        foro.id_ultimo_tema, foro.nom_ultimo_tema
        FROM foros AS foro
        WHERE foro.id_categoria =".$this->get_id_categoria()."
        ORDER BY foro.id_foro";
      $this->Squery();
    }
    ##########################################################################
    ### Se encarga de declarar las variables de $_SESSION[] de los foros
    public function ALL_FORO($forzado = false){
      $this->get();
      $ALL_FORO = array();
      $foro = $this->get_data();
      $this->set_data();
      if($forzado){
        foreach ($foro as $key => $value) {
          $ALL_FORO[$value['id_foro']] = $value;
        }
        $_SESSION['foro'] = $ALL_FORO;
      }else{
        $cantidad_foro = count($foro);
        if(!isset($_SESSION['cantidad_foro'])){
          $_SESSION['cantidad_foro'] = $cantidad_foro;
        }
        if($_SESSION['cantidad_foro'] != $cantidad_foro){
          foreach ($foro as $key => $value) {
            $ALL_FORO[$value['id_foro']] = $value;
          }
          $_SESSION['cantidad_foro'] = $cantidad_foro;
          $_SESSION['foro'] = $ALL_FORO;
        } else {
          if (!isset($_SESSION['foro'])) {
            foreach ($foro as $key => $value) {
              $ALL_FORO[$value['id_foro']] = $value;
            }
            $_SESSION['foro'] = $ALL_FORO;
          }
        }
      }
    }
    ##################### FIN #########################
}
?>
