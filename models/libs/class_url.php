<?php
#### Se enfocara en remplazar y crear la url amigable los nombres solamente #####

class class_url_amigable {
      protected $id_1;
      protected $titulo;
      protected $id_2;

      public function set_id_1($id_1= ""){
        $this->id_1 = $id_1;
      }
      public function set_titulo($titulo = ""){
        $this->titulo = $titulo;
      }
      public function set_id_2($id_2 = ""){
        $this->id_2 = $id_2;
      }
      public function reset(){
        $this->id_1 = "";
        $this->id_2 = "";
        $this->titulo = "";
      }

      #######################METODOS PRINCIPALES ##########################
      public function url_amigable() {
        if(empty($this->id_2)){
          $titulo = $this->id_1 . '-' . $this->titulo;
        }else{
          $titulo = $this->id_1 . '-' . $this->id_2 . '-' . $this->titulo;
        }
        $titulo = trim($titulo);
        $titulo = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            'a',
            $titulo
        );
        $titulo = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            'e',
            $titulo
        );
        $titulo = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            'i',
            $titulo
        );
        $titulo = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            'o',
            $titulo
        );
        $titulo = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            'u',
            $titulo
        );
        $titulo = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'n', 'c', 'c',),
            $titulo
        );
          $titulo = str_replace(
              array("\\", "¨", "º", "-", "~",
                   "#", "@", "|", "!", "\"",
                   "·", "$", "%", "&", "/",
                   "(", ")", "?", "'", "¡",
                   "¿", "[", "^", "`", "]",
                   "+", "}", "{", "¨", "´",
                   ">", "< ", ";", ",", ":",
                   ".", " "),
              '-',
              $titulo
          );
        $titulo =strtolower($titulo);
        return $titulo;
      }
      ##################### FIN #########################

      ########################## CONSTRUCTOR Y DESTRUCTOR ################################
      function __construct($id_1= "",$titulo = ""){
          $this->id_1 = $id_1;
          $this->titulo =$titulo;
      }
      function __destruct(){
          unset($this);
      }
      ##################### FIN #########################
  }
?>
