<?php
class tema_model extends database {
    private $id_tema;
    private $nombre;
    private $contenido;
    private $id_foro;
    private $id_user_d;
    private $estado;
    private $tipo;
    private $fecha_creacion;
    private $visitas;
    private $respuestas;
    private $id_user_u;
    private $fecha_ultimo;


    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'foro';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################

    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_id_tema($id_tema){
        $this->id_tema = $id_tema;
    }
    public function get_id_tema(){
        return $this->id_tema;
    }
    public function set_nombre($nombre){
      $this->nombre = $nombre;
    }
    public function get_nombre(){
        return $this->nombre;
    }
    public function set_contenido($contenido){
      $this->contenido = $contenido;
    }
    public function get_contenido(){
        return $this->contenido;
    }
    public function set_id_foro($id_foro){
      $this->id_foro = $id_foro;
    }
    public function get_id_foro(){
        return $this->id_foro;
    }
    public function set_id_user_d($id_user_d){
      $this->id_user_d = $id_user_d;
    }
    public function get_id_user_d(){
        return $this->id_user_d;
    }
    public function set_estado($estado){
      $this->estado = $estado;
    }
    public function get_estado(){
        return $this->estado;
    }
    public function set_tipo($tipo){
      $this->tipo = $tipo;
    }
    public function get_tipo(){
        return $this->tipo;
    }
    public function set_fecha_creacion($fecha_creacion){
      $this->fecha_creacion = $fecha_creacion;
    }
    public function get_fecha_creacion(){
        return $this->fecha_creacion;
    }
    public function set_visitas($visitas){
      $this->visitas = $visitas;
    }
    public function get_visitas(){
        return $this->visitas;
    }
    public function set_respuestas($respuestas){
      $this->respuestas = $respuestas;
    }
    public function get_respuestas(){
        return $this->respuestas;
    }
    public function set_id_user_u($id_user_u){
      $this->id_user_u = $id_user_u;
    }
    public function get_id_user_u(){
        return $this->id_user_u;
    }
    public function set_fecha_ultimo($fecha_ultimo){
      $this->fecha_ultimo = $fecha_ultimo;
    }
    public function get_fecha_ultimo(){
        return $this->fecha_ultimo;
    }

    public function set_data($data =[]){
      $this->data = $data;
    }
    public function get_data(){
        return $this->data;
    }
    public function get_last_id(){
        return $this->last_id;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################
    ########## PAGINA DE AGREGAR TEMA #############
    ##### Agrega el tema
    public function set($anuncio = false){
        $this->sql = "
        INSERT INTO temas SET
        nombre = '".$this->get_nombre()."',
        contenido = '".$this->get_contenido()."',
        id_foro ='".$this->get_id_foro()."',
        id_user_d ='".$this->get_id_user_d()."',
        id_user_u ='".$this->get_id_user_u()."',
        fecha_creacion ='".$this->get_fecha_creacion()."',
        fecha_ultimo ='".$this->get_fecha_ultimo()."'";
        if ($anuncio) {
          $this->sql .= ',tipo = 1';
        }
        $this->IUDquery();
    }
    #########################################
    ########## PAGINA TEMA DE FORO ###############
    ### Obtiene los temas correspondientes del foro
    public function get_tema(){
      $this->sql = "
        SELECT tema.estado, tema.nombre,
        tema.id_user_d, tema.visitas, tema.respuestas,
        tema.id_user_u, tema.fecha_ultimo, tema.id_tema
        FROM temas AS tema
        WHERE tema.tipo = ".$this->get_tipo()."
        AND tema.id_foro = ".$this->get_id_foro()." ORDER BY tema.fecha_creacion";
      $this->Squery();
    }
    #################################################
    ########## PAGINA PERFIL ###############
    ### Obtiene los temas por usuario
    public function get_tema_user(){
      $this->sql = "
        SELECT COUNT(id_tema) as temas
        FROM temas AS tema
        WHERE tema.id_user_d = ".$this->get_id_user_d();
      $this->Squery();
    }
    #################################################
    ############ PAGINA TEMA #######################
    ##### Obtiene el tema a visualizar
    public function get_tema_id(){
        $this->sql = "
        SELECT * FROM temas
        WHERE id_tema ='".$this->get_id_tema()."'";
        $this->Squery();
    }
    ####################################################
    ##### Actualiza el numero de visitas de la pagina
    public function update_visitas(){
        $this->sql = "
        UPDATE temas SET
        visitas = visitas + '1'
        WHERE id_tema ='".$this->get_id_tema()."'";
        $this->IUDquery();
    }
    ####################################################
    ##### Actualiza el estado del tema
    public function update_estado(){
        $this->sql = "
        UPDATE temas SET
        estado = '".$this->get_estado()."'
        WHERE id_tema ='".$this->get_id_tema()."'";
        $this->IUDquery();
    }
    #####################################################
    ##### Elimina el tema
    public function delete(){
        $this->sql = "
        DELETE FROM temas
        WHERE id_tema ='".$this->get_id_tema()."'";
        $this->IUDquery();
    }
    ######################################################
    ##### Obtiene el ultimo tema creado
    public function get_ultimo_tema(){
        $this->sql = "
        SELECT id_tema,nombre FROM temas ORDER BY fecha_ultimo DESC LIMIT 1";
        $this->Squery();
    }
    ######################################################
    ##### Actualiza el tema
    public function update($anuncio = false){
      $this->sql = "
        UPDATE temas SET
        nombre = '".$this->get_nombre()."',
        contenido = '".$this->get_contenido()."'";
      if ($anuncio) {
        $this->sql .= ',tipo = 1';
      }else {
        $this->sql .= ',tipo = 0';
      }
      $this->sql .= " WHERE id_tema ='".$this->get_id_tema()."'";
      $this->IUDquery();
    }
    ########################################################
    ######################### RESPONDER TEMA ########################
    ###### Actualiza la cantidad de respuestas
    public function update_respuesta(){
      $this->sql = "
        UPDATE temas SET
        respuestas = respuestas + 1
        WHERE id_tema ='".$this->get_id_tema()."'";
      $this->IUDquery();
    }
    ########################################################################
    ######################### ELIMINAR RESPUESTA ########################
    ###### Actualiza la cantidad de respuestas
    public function update_eliminar_respuesta(){
      $this->sql = "
        UPDATE temas SET
        respuestas = respuestas - 1
        WHERE id_tema ='".$this->get_id_tema()."'";
      $this->IUDquery();
    }
    ########################################################################

    public function get(){
        $this->sql = "
        SELECT * FROM temas";
        $this->Squery();
    }
    ##################### FIN #########################
}
?>
