<?php
class responder_model extends database {
    private $id_tema;
    private $contenido;
    private $id_user;
    private $fecha_creacion;
    private $id_respuesta;

    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'foro';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################

    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_id_respuesta($id_respuesta){
        $this->id_respuesta = $id_respuesta;
    }
    public function get_id_respuesta(){
        return $this->id_respuesta;
    }
    public function set_id_tema($id_tema){
        $this->id_tema = $id_tema;
    }
    public function get_id_tema(){
        return $this->id_tema;
    }
    public function set_contenido($contenido){
      $this->contenido = $contenido;
    }
    public function get_contenido(){
        return $this->contenido;
    }
    public function set_id_user($id_user){
      $this->id_user = $id_user;
    }
    public function get_id_user(){
        return $this->id_user;
    }
    public function set_fecha_crea($fecha_crea){
      $this->fecha_crea = $fecha_crea;
    }
    public function get_fecha_crea(){
        return $this->fecha_crea;
    }

    public function set_data($data =[]){
      $this->data = $data;
    }
    public function get_data(){
        return $this->data;
    }
    public function get_last_id(){
        return $this->last_id;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################
    ########## PAGINA DE RESPONDER TEMA #############
    ##### Agrega la respuesta
    public function set(){
      $this->sql = "
        INSERT INTO respuestas SET
        contenido = '".$this->get_contenido()."',
        id_tema ='".$this->get_id_tema()."',
        id_user ='".$this->get_id_user()."',
        fecha_crea ='".$this->get_fecha_crea()."'";
      $this->IUDquery();
    }
    #########################################
    ########## PAGINA MOSTRAR TEMA DE FORO ###############
    ### Obtiene las respuestas correspondientes del tema
    public function get(){
      $this->sql = "
        SELECT * FROM respuestas
        WHERE id_tema = ".$this->get_id_tema()." ORDER BY fecha_crea DESC";
      $this->Squery();
    }
    #################################################
    ########## PAGINA EDITAR RESPUESTA ###############
    ### Obtiene una respuesta
    public function get_respuesta(){
      $this->sql = "
        SELECT * FROM respuestas
        WHERE id_respuesta = ".$this->get_id_respuesta();
      $this->Squery();
    }
    ########### ELIMINAR TEMA #######################
    #### Obtiene los usuarios que contestaron en un tema
    public function get_respuesta_users(){
      $this->sql = "
        SELECT DISTINCT id_user FROM respuestas
        WHERE id_tema = ".$this->get_id_tema();
      $this->Squery();
    }
    #########################################################
    #### Cuenta las respuestas que ha hecho un usuario
    public function get_mensajes_user(){
      $this->sql = "
        SELECT COUNT(id_respuesta) AS mensajes FROM respuestas
        WHERE id_user =".$this->get_id_user()."";
      $this->Squery();
    }
    #########################################################
    #################### FIN #############################

    ###################### PAGINA EDITAR RESPUESTA ####################
    ########### Actualiza la respuesta
    public function update(){
      $this->sql = "
        UPDATE respuestas SET
        contenido = '".$this->get_contenido()."',
        fecha_crea = '".$this->get_fecha_crea()."'
        WHERE id_respuesta = '".$this->get_id_respuesta()."'";
      $this->IUDquery();
    }
    #########################################################
    public function delete(){
      $this->sql = "
        DELETE FROM respuestas
        WHERE id_respuesta = '".$this->get_id_respuesta()."'";
      $this->IUDquery();
    }
    ##################### FIN #########################
}
?>
