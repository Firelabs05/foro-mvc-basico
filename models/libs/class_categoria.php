<?php
class categoria_model extends database {
    private $id_categoria;
    private $nombre;
    private $descripcion;

    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'foro';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################

    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_id_categoria($id_categoria){
        $this->id_categoria = $id_categoria;
    }
    public function get_id_categoria(){
        return $this->id_categoria;
    }
    public function set_nombre($nombre){
      $this->nombre = $nombre;
    }
    public function get_nombre(){
        return $this->nombre;
    }
    public function set_descripcion($descripcion){
      $this->descripcion = $descripcion;
    }
    public function get_descripcion(){
        return $this->descripcion;
    }

    public function get_data(){
        return $this->data;
    }
    public function set_data($data = []){
        $this->data = $data;
    }
    public function get_last_id(){
        return $this->last_id;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################

    ### Agrega la informacion de las categorias
    public function set(){
      $this->sql = "
        INSERT INTO categorias SET
        nombre = '".$this->get_nombre()."',
        descripcion = '".$this->get_descripcion()."'";
      $this->IUDquery();
    }
    ######################################################
    ### Borra a las categorias
    public function delete(){
        $this->sql = "
        DELETE FROM categorias
        WHERE id_categoria ='".$this->get_id_categoria()."'";
        $this->IUDquery();
    }
    ######################################################
    ### Edita las categorias
    public function update(){
      $this->sql = "
        UPDATE categorias SET
        nombre = '".$this->get_nombre()."',
        descripcion = '".$this->get_descripcion()."'
        WHERE id_categoria ='".$this->get_id_categoria()."'";
      $this->IUDquery();
    }
    ######################################################
    ### Obtiene todas las categorias
    public function get(){
      $this->sql = "
        SELECT * FROM categorias ORDER BY id_categoria";
      $this->Squery();
    }
    ######################################################

    ### Se encarga de declarar las variables de $_SESSION[] de las categorias
    public function ALL_CATEGORY($forzado = false){
      $this->get();
      $ALL_CATEGORIA = array();
      $categoria = $this->get_data();
      $this->set_data();
      if($forzado){
        foreach ($categoria as $key => $value) {
          $ALL_CATEGORIA[$value['id_categoria']] = $value;
        }
        $_SESSION['category'] = $ALL_CATEGORIA;
      }else{
        $cantidad_categoria = count($categoria);
        if(!isset($_SESSION['cantidad_categoria'])){
          $_SESSION['cantidad_categoria'] = $cantidad_categoria;
        }
        if($_SESSION['cantidad_categoria'] != $cantidad_categoria){
          foreach ($categoria as $key => $value) {
            $ALL_CATEGORIA[$value['id_categoria']] = $value;
          }
          $_SESSION['cantidad_categoria'] = $cantidad_categoria;
          $_SESSION['category'] = $ALL_CATEGORIA;
        } else {
          if (!isset($_SESSION['category'])) {
            foreach ($categoria as $key => $value) {
              $ALL_CATEGORIA[$value['id_categoria']] = $value;
            }
            $_SESSION['category'] = $ALL_CATEGORIA;
          }
        }
      }
    }
    ##################### FIN #########################
}
?>
