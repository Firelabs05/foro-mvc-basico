<?php
class user_model extends database {
    private $id_user;
    private $user;
    private $password;
    private $email;
    private $permiso;
    private $keyreg;
    private $activo;
    private $keypass;
    private $newpass;
    private $img;
    private $firma;
    private $rango;
    private $edad;
    private $fecha_reg;
    private $biografia;
    private $ultima_con;
    private $mensajes;

    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'foro';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################

    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_id_user($id_user){
        $this->id_user = $id_user;
    }
    public function get_id_user(){
        return $this->id_user;
    }
    public function set_user($user){
      $this->user = $user;
    }
    public function get_user(){
        return $this->user;
    }
    public function set_password($password){
      $this->password = $password;
    }
    public function get_password(){
        return $this->password;
    }
    public function set_email($email){
      $this->email = $email;
    }
    public function get_email(){
        return $this->email;
    }
    public function set_permiso($permiso){
      $this->permiso = $permiso;
    }
    public function get_permiso(){
        return $this->permiso;
    }
    public function set_keyreg($keyreg){
      $this->keyreg = $keyreg;
    }
    public function get_keyreg(){
        return $this->keyreg;
    }
    public function set_activo($activo){
      $this->activo = $activo;
    }
    public function get_activo(){
        return $this->activo;
    }
    public function set_keypass($keypass){
      $this->keypass = $keypass;
    }
    public function get_keypass(){
        return $this->keypass;
    }
    public function set_newpass($newpass){
      $this->newpass = $newpass;
    }
    public function get_newpass(){
        return $this->newpass;
    }
    public function set_img($img){
      $this->img = $img;
    }
    public function get_img(){
        return $this->img;
    }
    public function set_firma($firma){
      $this->firma = $firma;
    }
    public function get_firma(){
        return $this->firma;
    }
    public function set_rango($rango){
      $this->rango = $rango;
    }
    public function get_rango(){
        return $this->rango;
    }
    public function set_edad($edad){
      $this->edad = $edad;
    }
    public function get_edad(){
        return $this->edad;
    }
    public function set_fecha_reg($fecha_reg){
      $this->fecha_reg = $fecha_reg;
    }
    public function get_fecha_reg(){
        return $this->fecha_reg;
    }
    public function set_biografia($biografia){
      $this->biografia = $biografia;
    }
    public function get_biografia(){
        return $this->biografia;
    }
    public function set_ultima_con($ultima_con){
      $this->ultima_con = $ultima_con;
    }
    public function get_ultima_con(){
        return $this->ultima_con;
    }
    public function set_mensajes($mensajes){
      $this->mensajes = $mensajes;
    }
    public function get_mensajes(){
        return $this->mensajes;
    }

    public function get_data(){
        return $this->data;
    }
    public function set_data($data = []){
        $this->data = $data;
    }
    public function get_last_id(){
        return $this->last_id;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################
    public function delete(){
        $this->sql = "
        DELETE FROM users
        WHERE id_user ='".$this->get_id_user()."'";
        $this->IUDquery();
    }
    public function edit(){
        $this->sql = "
        SELECT * FROM users
        WHERE id_user ='".$this->get_id_user()."'";
        $this->Squery();
    }
    public function update(){
        $this->sql = "
        UPDATE users SET
        user = '".$this->get_user()."',
        password = '".$this->get_password()."',
        email = '".$this->get_email()."',
        permiso = '".$this->get_permiso()."'
        WHERE id_user ='".$this->get_id_user()."'";
        $this->IUDquery();
    }
    #### Obtiene todos los usuarios
    public function get(){
        $this->sql = "
        SELECT * FROM users";
        $this->Squery();
    }
    ###########################################

    ############################ LOGIN Y REGISTRO ##########################

    ############# LOGIN ###############
    ### Obtiene el id_user y el password, dependiendo del usuario
    public function get_login(){
      $this->sql = "
        SELECT id_user, user, password FROM users
        WHERE user ='".$this->get_user()."' OR
        email ='".$this->get_email()."' LIMIT 1";
      $this->Squery();
    }
    #############################################################
    ###### Actualiza la ultima conexion del usuario
    public function update_ultima_con(){
      $this->sql = "
        UPDATE users SET
        ultima_con = '".$this->get_ultima_con()."'
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ################################################################
    #############  FIN LOGIN ###############

    #############  REGISTRO #########################
    ####### Agrega la informacion del usuario
    public function set(){
      $this->sql = "
        INSERT INTO users SET
        user = '".$this->get_user()."',
        password = '".$this->get_password()."',
        email = '".$this->get_email()."',
        keyreg = '".$this->get_keyreg()."',
        fecha_reg = '".$this->get_fecha_reg()."'";
      $this->IUDquery();
    }
    ################ FIN REGISTRO ###################

    ####################### FIN LOGIN Y REGISTRO #######################

    ###################### ACTIVACION DE LA CUENTA ########################
    ### Obtiene el id_user, revisando que si exista el keyreg
    public function get_activacion(){
      $this->sql = "
        SELECT id_user FROM users
        WHERE id_user ='".$this->get_id_user()."' AND
        keyreg ='".$this->get_keyreg()."' LIMIT 1";
      $this->Squery();
    }
    ##############################################################
    ### Actualiza los campos para validar la activacion de la cuenta
    public function update_activacion(){
      $this->sql = "
        UPDATE users SET
        activo = '".$this->get_activo()."',
        keyreg = '".$this->get_keyreg()."'
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ###############################################################
    #################### FIN DE ACTIVACION DE LA CUENTA ####################

    #################### RECUPERAR PASSWORD #############################
    ##### Busca si el email existe
     public function get_recuperar_password(){
       $this->sql = "
          SELECT id_user,user FROM users
          WHERE email ='".$this->get_email()."' LIMIT 1";
       $this->Squery();
    }
    ######################################################
    ##### Actualiza y introduce el password ficticio
    public function update_recuperar_password(){
      $this->sql = "
        UPDATE users SET
        newpass = '".$this->get_newpass()."',
        keypass = '".$this->get_keypass()."'
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ######################################################
    ##### Busca si existe el usuario con ese keypass
    public function get_recuperar_keypass(){
      $this->sql = "
        SELECT id_user,newpass,email,user FROM users
        WHERE keypass ='".$this->get_keypass()."' LIMIT 1";
      $this->Squery();
    }
    ######################################################
    ##### Actualiza la contraseña
    public function update_password(){
      $this->sql = "
        UPDATE users SET
        password = '".$this->get_password()."',
        newpass = '',
        keypass = ''
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ######################################################
    ######################## FIN RECUPERAR PASSWORD ###################

    ######################### RESPONDER TEMA ###########################
    ###### Actualiza la cantidad de respuestas
    public function update_respuesta(){
      $this->sql = "
        UPDATE users SET
        mensajes = mensajes + 1
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ########################################################################
    ######################### ELIMIANR RESPUESTA ###########################
    ###### Actualiza la cantidad de respuestas
    public function update_eliminar_respuesta(){
      $this->sql = "
        UPDATE users SET
        mensajes = mensajes - 1
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ########################################################################
    ########################## ELIMINAR TEMA ##############################
    public function update_mensajes(){
      $this->sql = "
        UPDATE users SET
        mensajes = '".$this->get_mensajes()."'
        WHERE id_user ='".$this->get_id_user()."'";
      $this->IUDquery();
    }
    ##################### FIN #########################

    ####################### METODOS GENERALES ###############
    public function ALL_USERS($forzado = false){
      $this->get();
      $ALL_USER = array();
      $users = $this->get_data();
      $cantidad_users = count($users);
      if ($forzado) {
        foreach ($users as $key => $value) {
          $ALL_USER[$value['id_user']] = $value;
        }
        $_SESSION['cantidad_user'] = $cantidad_users;
        $_SESSION['users'] = $ALL_USER;
      } else {
        if(!isset($_SESSION['cantidad_user'])){
          $_SESSION['cantidad_user'] = $cantidad_users;
        }
        if($_SESSION['cantidad_user'] != $cantidad_users){
          foreach ($users as $key => $value) {
            $ALL_USER[$value['id_user']] = $value;
          }
          $_SESSION['cantidad_user'] = $cantidad_users;
          $_SESSION['users'] = $ALL_USER;
        } else {
          if (!isset($_SESSION['users'])) {
            foreach ($users as $key => $value) {
              $ALL_USER[$value['id_user']] = $value;
            }
            $_SESSION['users'] = $ALL_USER;
          }
        }
      }
    }
    ######################### FIN ########################
}
?>
