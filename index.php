<?php
require_once('models/config.php'); // Libreria con todos los documentos

if (isset($_GET['view']) && !empty($_GET['view'])){
  $view = strtolower($_GET['view']);
    ################# REDIRIGE A SU CONTROLADOR #########################3
  if(file_exists('controllers/'.$view.'_controller.php')){
    include('controllers/'.$view.'_controller.php');
    ######################## FIN #########################
  } else {
    ############## MUESTRA PAGINA DE ERRORES 404 ######################
    header('location: ?view=error_404');
  }
  ################### FIN DE ERROR 404 ##################################
}else {
  // En caso de que no exista alguna vista por default manda a index
  header('location: ?view=index');
}
?>
