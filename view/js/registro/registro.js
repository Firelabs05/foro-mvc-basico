__('form_registro').addEventListener('keypress',scrip_registro,false);
__('iniciar_registro').addEventListener('click',registro,false);

function registro() {
  var user, email, password, password2, tyc, valor, error;
  password = __('password_registro').value;
  password2 = __('password_registro_2').value;
  user = __('user_registro').value;
  email = __('email_registro').value
  tyc = __('tyc').checked ? true : false;
  error= "Por favor rellene este campo";
  if(password != "" && password.length > 5
     && password2 != "" && user!=""
     && password.length > 5 && email!=""
     && tyc != false){
    __RDI('user_registro');
    __RDE('user_registro_div_alert');
    __RDI('email_registro');
    __RDE('email_registro_div_alert');
    __RDI('password_registro');
    __RDE('password_registro_div_alert');
    __RDI('password_registro_2');
    __RDE('password_registro_2_div_alert');
    __RDI('tyc');
    __RDE('tyc_registro_div_alert');

    if(password === password2){
        valor = "user="+user+"&password="+password+"&email="+email+"&mode=registro";
        inicio_ajax(respuesta_registro,valor,cargando_registro);
    }else{
      __ADI('password_registro_2');
      __ADE('password_registro_2_div_alert',"La contraseña no coincide");
    }
  }else{
    if (user == "") {
      __ADI('user_registro');
      __ADE('user_registro_div_alert',error);
    } else {
      if(user.length < 6){
        __RDI('user_registro');
        __RDE('user_registro_div_alert');
        __ADI('user_registro');
        __ADE('user_registro_div_alert','El usuario debe tener minimo 5 caracteres');
      }else {
        __RDI('user_registro');
        __RDE('user_registro_div_alert');
      }
    }
    if (email == "") {
      __ADI('email_registro');
      __ADE('email_registro_div_alert',error);
    } else {
      __RDI('email_registro');
      __RDE('email_registro_div_alert');
    }
    if (password == "") {
      __ADI('password_registro');
      __ADE('password_registro_div_alert',error);
    } else {
      if (password.length < 6) {
        __RDI('password_registro');
        __RDE('password_registro_div_alert');
        __ADI('password_registro');
        __ADE('password_registro_div_alert','La contraseña debe de tener minimo 5 caracteres');
      } else {
        __RDI('password_registro');
        __RDE('password_registro_div_alert');
      }
    }
    if (password2 == "") {
      __ADI('password_registro_2');
      __ADE('password_registro_2_div_alert',error);
    } else {
      __RDI('password_registro_2');
      __RDE('password_registro_2_div_alert');
    }
    if (tyc == false) {
      __ADI('tyc');
      __ADE('tyc_registro_div_alert',error);
    } else {
      __RDI('tyc');
      __RDE('tyc_registro_div_alert');
    }
  }
}
function respuesta_registro(){
  if(this.peticion.responseText == 1){
    location.reload();
  }else{
    __('alert_registro').innerHTML = this.peticion.responseText;
  }
}
function scrip_registro(tecla) {
  if(tecla.keyCode == 13){
    registro();
  }
}
function cargando_registro() {
  var alerta = '<div class="alert alert-dismissible alert-info">'
              +'<button type="button" class="close" data-dismiss="alert">&times;</button>'
              +'<strong>Tenemos a nuestros esclavos trabajando, por favor espere  </strong>'
              +'</div>';

  __('alert_registro').innerHTML = alerta;
}
