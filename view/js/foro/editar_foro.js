__('form_editar_foro').addEventListener('keypress',scrip_editar_foro,false);
__('iniciar_editar_foro').addEventListener('click',editar_foro,false);
function editar_foro() {
  var foro, descripcion, categoria, estado, error;
  foro = __('foro_editar_foro').value;
  descripcion = __('descripcion_editar_foro').value;
  categoria = __('categoria_editar_foro').value;
  estado = __NAME('estado_foro')[0].checked ? 1 : 0;
  error = "Por favor rellene este campo";
  if(foro !="" && foro.length >= 8
    && descripcion != "" && descripcion.length >= 25
    && categoria != 0){
    __RDI('foro_editar_foro');
    __RDE('foro_editar_foro_div_alert');
    __RDI('descripcion_editar_foro');
    __RDE('descripcion_editar_foro_div_alert');
    __RDI('categoria_editar_foro');
    __RDE('categoria_editar_foro_div_alert');
    __('form_editar_foro').submit();

  }else{
    if(foro == ""){
      __ADI('foro_editar_foro');
      __ADE('foro_editar_foro_div_alert',error);
    }else{
      if(foro.length < 8){
        __RDI('foro_editar_foro');
        __RDE('foro_editar_foro_div_alert');
        __ADI('foro_editar_foro');
        __ADE('foro_editar_foro_div_alert','La cantidad minima debe tener 8 caracteres');
      }else {
        __RDI('foro_editar_foro');
        __RDE('foro_editar_foro_div_alert');
      }
    }
    if (descripcion == "") {
      __ADI('descripcion_editar_foro');
      __ADE('descripcion_editar_foro_div_alert',error);
    } else {
      if (descripcion.length < 25) {
        __RDI('foro_editar_foro');
        __RDE('foro_editar_foro_div_alert');
        __ADI('foro_editar_foro');
        __ADE('foro_editar_foro_div_alert','La cantidad minima debe tener 25 caracteres');
      } else {
        __RDI('descripcion_editar_foro');
        __RDE('descripcion_editar_foro_div_alert');
      }
    }
    if (categoria == 0) {
      __ADI('categoria_editar_foro');
      __ADE('categoria_editar_foro_div_alert',"Debe de crear al menos una categoria antes de proseguir");
    } else {
      __RDI('categoria_editar_foro');
      __RDE('categoria_editar_foro_div_alert');
    }
  }
}
function scrip_editar_foro(tecla) {
  if(tecla.keyCode == 13){
    agregar_foro();
  }
}
