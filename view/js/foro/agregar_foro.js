__('form_agregar_foro').addEventListener('keypress',scrip_agregar_foro,false);
__('iniciar_agregar_foro').addEventListener('click',agregar_foro,false);
function agregar_foro() {
  var foro, descripcion, categoria, estado, error;
  foro = __('foro_agregar_foro').value;
  descripcion = __('descripcion_agregar_foro').value;
  categoria = __('categoria_agregar_foro').value;
  estado = __NAME('estado_foro')[0].checked ? 1 : 0;
  error = "Por favor rellene este campo";
  if(foro !="" && foro.length >= 8
    && descripcion != "" && descripcion.length >= 25
    && categoria != 0){
    __RDI('foro_agregar_foro');
    __RDE('foro_agregar_foro_div_alert');
    __RDI('descripcion_agregar_foro');
    __RDE('descripcion_agregar_foro_div_alert');
    __RDI('categoria_agregar_foro');
    __RDE('categoria_agregar_foro_div_alert');

    __('form_agregar_foro').submit();
  }else{
    if(foro == ""){
      __ADI('foro_agregar_foro');
      __ADE('foro_agregar_foro_div_alert',error);
    }else{
      if(foro.length < 8) {
        __RDI('foro_agregar_foro');
        __RDE('foro_agregar_foro_div_alert');
        __ADI('foro_agregar_foro');
        __ADE('foro_agregar_foro_div_alert','El nombre debe de tener al menos 8 caracteres');
      } else {
        __RDI('foro_agregar_foro');
        __RDE('foro_agregar_foro_div_alert');
      }

    }
    if (descripcion == "") {
      __ADI('descripcion_agregar_foro');
      __ADE('descripcion_agregar_foro_div_alert',error);
    } else {
      if(descripcion.length < 25){
        __RDI('descripcion_agregar_foro');
        __RDE('descripcion_agregar_foro_div_alert');
        __ADI('descripcion_agregar_foro');
        __ADE('descripcion_agregar_foro_div_alert','La descripcion debe de tener al menos 25 caracteres');
      }else{
        __RDI('descripcion_agregar_foro');
        __RDE('descripcion_agregar_foro_div_alert');
      }
    }
    if (categoria == 0) {
      __ADI('categoria_agregar_foro');
      __ADE('categoria_agregar_foro_div_alert',"Debe de crear al menos una categoria antes de proseguir");
    } else {
      __RDI('categoria_agregar_foro');
      __RDE('categoria_agregar_foro_div_alert');
    }
  }
}
function scrip_agregar_foro(tecla) {
  if(tecla.keyCode == 13){
    agregar_foro();
  }
}
