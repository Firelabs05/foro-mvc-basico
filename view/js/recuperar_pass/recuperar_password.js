__('form_recuperar_password').addEventListener('keypress',scrip_recuperar_password,false);
__('iniciar_recuperar_password').addEventListener('click',recuperar_password,false);

function recuperar_password() {
  var email, valor,error;
  email = __('email_recuperar_password').value
  error = "Por favor rellene este campo"
  if(email!=""){
    __RDI('email_recuperar_password');
    __RDE('email_recuperar_password_div_alert');
    valor = "email="+email+"&mode=recuperar_password";
    inicio_ajax(respuesta_recuperar_password,valor,cargando_recuperar_password);
  }else{
    __ADI('email_recuperar_password');
    __ADE('email_recuperar_password_div_alert',error);
  }
}
function respuesta_recuperar_password(){
  if(this.peticion.responseText == 1){
    location.reload();
  }else{
    __('alert_recuperar_password').innerHTML = this.peticion.responseText;
  }
}
function scrip_recuperar_password(tecla) {
  if(tecla.keyCode == 13){
    recuperar_password();
  }
}
function cargando_recuperar_password() {
  var alerta = '<div class="alert alert-dismissible alert-info">'
              +'<button type="button" class="close" data-dismiss="alert">&times;</button>'
              +'<strong>Tenemos a nuestros esclavos trabajando, por favor espere  </strong>'
              +'</div>';

  __('alert_recuperar_password').innerHTML = alerta;
}
