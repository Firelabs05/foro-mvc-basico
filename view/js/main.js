//############## Funciones Generales #################
// Devuelve un nodo del DOM
function __(id){
  return document.getElementById(id);
}
function __NAME(name) {
  return document.getElementsByName(name);
}
// Convierte un input en rojo para error
// Cambia de color al input
function __ADI(id) {
  __(id).classList.add("is-invalid");
}
// Añade un msj de error abajo del input
function __ADE(id,error) {
  __(id).classList.add("invalid-feedback");
  __(id).innerHTML = error;
}

// Convierte un input rojo en normal
// Regresa el color del input a su valor normal
function __RDI(id) {
  __(id).classList.remove("is-invalid");
}
// Elimina le mensaje
function __RDE(id) {
  __(id).classList.remove("invalid-feedback");
  __(id).innerHTML = ""
}
