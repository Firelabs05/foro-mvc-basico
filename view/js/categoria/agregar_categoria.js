__('form_agregar_categoria').addEventListener('keypress',scrip_agregar_categoria,false);
__('iniciar_agregar_categoria').addEventListener('click',agregar_categoria,false);
function agregar_categoria() {
  var categoria, descripcion, error;
  categoria = __('categoria_agregar_categoria').value;
  descripcion = __('descripcion_agregar_categoria').value;
  error = "Por favor rellene este campo";
  if(categoria !="" && categoria.length >7
     && descripcion != "" && descripcion.length > 24){
    __('form_agregar_categoria').submit();
  }else{
    if(categoria == ""){
      __ADI('categoria_agregar_categoria');
      __ADE('categoria_agregar_categoria_div_alert',error);
    }else{
      if(categoria.length <= 7){
        __RDI('categoria_agregar_categoria');
        __RDE('categoria_agregar_categoria_div_alert');
        __ADI('categoria_agregar_categoria');
        __ADE('categoria_agregar_categoria_div_alert','El titulo debe tener 8 caracteres como minimo');
      }else {
        __RDI('categoria_agregar_categoria');
        __RDE('categoria_agregar_categoria_div_alert');
      }
    }
    if (descripcion == "") {
      __ADI('descripcion_agregar_categoria');
      __ADE('descripcion_agregar_categoria_div_alert',error);
    } else {
      if(descripcion.length <= 24){
        __RDI('descripcion_agregar_categoria');
        __RDE('descripcion_agregar_categoria_div_alert');
        __ADI('descripcion_agregar_categoria');
        __ADE('descripcion_agregar_categoria_div_alert','La descripcion debe tener como minimo 25 caracteres');
      } else {
        __RDI('descripcion_agregar_categoria');
        __RDE('descripcion_agregar_categoria_div_alert');
      }
    }
  }
}
function scrip_agregar_categoria(tecla) {
  if(tecla.keyCode == 13){
    agregar_categoria();
  }
}
