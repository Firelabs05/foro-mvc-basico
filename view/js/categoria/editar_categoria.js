__('form_editar_categoria').addEventListener('keypress',scrip_editar_categoria,false);
__('iniciar_editar_categoria').addEventListener('click',editar_categoria,false);

function editar_categoria() {
  var categoria, descripcion, error;
  categoria = __('categoria_editar_categoria').value
  descripcion = __('descripcion_editar_categoria').value
  error = "Por favor rellene este campo";
  if(categoria !="" && categoria.length >=8
     && descripcion != "" && descripcion.length >= 25){
      __RDI('categoria_editar_categoria');
      __RDE('categoria_editar_categoria_div_alert');
      __RDI('descripcion_editar_categoria');
      __RDE('descripcion_editar_categoria_div_alert');
      __('form_editar_categoria').submit();
  }else{
    if(categoria == ""){
      __ADI('categoria_editar_categoria');
      __ADE('categoria_editar_categoria_div_alert',error);
    }else{
      if(categoria.length <= 7){
        __RDI('categoria_editar_categoria');
        __RDE('categoria_editar_categoria_div_alert');
        __ADI('categoria_editar_categoria');
        __ADE('categoria_editar_categoria_div_alert','La cantidad minima de caracteres son 8');
      }else {
        __RDI('categoria_editar_categoria');
        __RDE('categoria_editar_categoria_div_alert');
      }
    }
    if (descripcion == "") {
      __ADI('descripcion_editar_categoria');
      __ADE('descripcion_editar_categoria_div_alert',error);
    } else {
      if(descripcion.length <= 24){
        __RDI('descripcion_editar_categoria');
        __RDE('descripcion_editar_categoria_div_alert');
        __ADI('descripcion_editar_categoria');
        __ADE('descripcion_editar_categoria_div_alert','La cantidad minima de caracteres son 25');
      }else {
        __RDI('descripcion_editar_categoria');
        __RDE('descripcion_editar_categoria_div_alert');
      }
    }
  }
}
function scrip_editar_categoria(tecla) {
  if(tecla.keyCode == 13){
    editar_categoria();
  }
}
