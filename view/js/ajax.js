var conexion = new Object();

conexion.NO_INICIADO=0;
conexion.CARGANDO=1;
conexion.CARGADO=2;
conexion.INTERACTUANDO=3;
conexion.COMPLETO=4;

conexion.cargaContenido = function(url,metodo,funcion,valor,cargando) {
    this.url = url;
    this.peticion = null;
    this.onload = funcion;
    if(cargando != undefined)
    {
        this.cargando = cargando;
    }
    this.solicitud(url,metodo,valor);
}

conexion.cargaContenido.prototype = {
    solicitud: function(url,metodo,valor) {
        this.peticion = new XMLHttpRequest();
        if(this.peticion) {
            try {
                var clon = this;
                this.peticion.onreadystatechange = function() {
                    clon.onReadyState.call(clon);
                }
                this.peticion.open(metodo, url, true);
                if(metodo == "POST")
                {
                    this.peticion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                }
                this.peticion.send(valor);
            } catch(err) {
                this.error.call(this);
            }
        }
    },
    onReadyState: function() {
        var peticion = this.peticion;
        var listo = peticion.readyState;
        if(listo == conexion.COMPLETO) {
            var estado = peticion.status;
            if(estado == 200 || estado == 0) {
                this.onload.call(this);
            }else {
                this.error.call(this);
            }
        }else if (listo == conexion.CARGANDO ||
                  listo == conexion.CARGADO ||
                  listo == conexion.INTERACTUANDO) {
            this.cargando.call(this);
        }
    },
    error: function() {
      alert('lacagste morro');
    }
}
function inicio_ajax(funcion_final,valor,cargando)
{
    if(cargando != undefined)
    {
        var cargador = new conexion.cargaContenido("http://localhost/Foro/ajax.php","POST",funcion_final,valor,cargando)
    }
    else
    {
        var cargador = new conexion.cargaContenido("http://localhost/Foro/ajax.php","POST",funcion_final,valor)
    }

}
