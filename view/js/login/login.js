__('form_login').addEventListener('keypress',scrip_login,false);
__('iniciar_sesion_login').addEventListener('click',login,false);

function login() {
  var user, password, sesion, valor, error;
  user = __('user_login').value;
  password = __('password_login').value;
  error = 'Por favor rellene este campo';
  if (user != "" && password != "") {
      __RDI('password_login');
      __RDE('password_login_div_alert');
      __RDI('user_login');
      __RDE('user_login_div_alert');
      sesion = __('sesion_login').checked ? true : false;
      valor = "user="+user+"&password="+password+"&sesion="+sesion+"&mode=login";
      inicio_ajax(respuesta_login,valor,cargando_login);
  }else{
    if (user == "") {
      __ADI('user_login');
      __ADE('user_login_div_alert',error);
    }else{
      __RDI('user_login');
      __RDE('user_login_div_alert');
    }
    if (password == "") {
      __ADI('password_login');
      __ADE('password_login_div_alert',error);
    }else{
      __RDI('password_login');
      __RDE('password_login_div_alert');
    }
  }
}
function respuesta_login(){
  if(this.peticion.responseText == 1){
    location.reload();
  }else{
    __('alert_login').innerHTML = this.peticion.responseText;
  }
}
function scrip_login(tecla) {
  if(tecla.keyCode == 13){
    login();
  }
}
function cargando_login() {
  var alerta = '<div class="alert alert-dismissible alert-info">'
              +'<button type="button" class="close" data-dismiss="alert">&times;</button>'
              +'<strong>Tenemos a nuestros esclavos trabajando, por favor espere  </strong>'
              +'</div>';

  __('alert_registro').innerHTML = alerta;
}
