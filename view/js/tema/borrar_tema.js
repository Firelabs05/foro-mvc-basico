window.addEventListener("load",iniciar,false);
function iniciar() {
  if (__('eliminar') != undefined) {
     __('eliminar').addEventListener('click',function () {
       eliminar_tema(this.value);
     } ,false);
  }
}
function eliminar_tema(value) {
  var valor = window.confirm("¿Estas seguro/a de eliminar este registro?");
  var foro  = __('id_foro').value;
  if (valor) {
    url = "?view=tema&mode=pagina_eliminar&id_foro="+ foro+"&id_tema="+value;
    window.location = url;
  }
}
