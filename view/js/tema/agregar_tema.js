__('iniciar_agregar_tema').addEventListener('click',agregar_tema,false);
function agregar_tema() {
  var tema, contenido, id_foro, valor, error_campo;
  tema = __('tema_agregar_tema').value;
  contenido = __('contenido_agregar_tema').value;
  error_campo = "Por favor rellene este campo";
  if(tema !="" && tema.length >= 9 &&
    contenido != "" && contenido.length >=270){
    __('form_agregar_tema').submit();
  }else{
    if(tema == ""){
      __ADI('tema_agregar_tema');
      __ADE('tema_agregar_tema_div_alert',error_campo);
    }else{
      if(tema.length < 9){
        __RDI('tema_agregar_tema');
        __RDE('tema_agregar_tema_div_alert');
        __ADI('tema_agregar_tema');
        __ADE('tema_agregar_tema_div_alert','El titulo debe tener minimo 9 caracteres');
      } else{
        __RDI('tema_agregar_tema');
        __RDE('tema_agregar_tema_div_alert');
      }
    }
    if (contenido == "") {
      __ADI('contenido_agregar_tema');
      __ADE('contenido_agregar_tema_div_alert',error_campo);
    } else {
      if(contenido.length < 270){
        __RDI('contenido_agregar_tema');
        __RDE('contenido_agregar_tema_div_alert');
        __ADI('contenido_agregar_tema');
        __ADE('contenido_agregar_tema_div_alert','El contenido debe de tener minimo 270 caracteres');
      } else {
        __RDI('contenido_agregar_tema');
        __RDE('contenido_agregar_tema_div_alert');
      }
    }
  }
}
