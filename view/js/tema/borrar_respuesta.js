window.addEventListener("load",iniciar,false);
function iniciar() {
  if (__NAME('eliminar_respuesta') != undefined) {
    var delet = __NAME('eliminar_respuesta');
    for (var f= 0; f<delet.length; f++ ) {
      delet[f].addEventListener('click',function () {
        eliminar_respuesta(this.value);
      } ,false);
    }
  }
}
function eliminar_respuesta(value) {
  var valor = window.confirm("¿Estas seguro/a de eliminar este registro?");
  var foro  = __('id_foro').value;
  var tema = __('id_tema').value;
  if (valor) {
    url = "?view=tema&mode=eliminar_respuesta&id_foro="+ foro+"&id_tema="+tema+"&id_respuesta="+value;
    window.location = url;
  }
}
