__('iniciar_responder_tema').addEventListener('click',responder_tema,false);
function responder_tema() {
  var contenido, valor, error_campo;
  contenido = __('contenido_responder_tema').value;
  error_campo = "Por favor rellene este campo";
  if(contenido != "" && contenido.length >=10){
    __('form_responder_tema').submit();
  }else{
    if (contenido == "") {
      __ADI('contenido_responder_tema');
      __ADE('contenido_responder_tema_div_alert',error_campo);
    } else {
      if(contenido.length < 10){
        __RDI('contenido_responder_tema');
        __RDE('contenido_responder_tema_div_alert');
        __ADI('contenido_responder_tema');
        __ADE('contenido_responder_tema_div_alert','El contenido debe de tener minimo 10 caracteres');
      } else {
        __RDI('contenido_responder_tema');
        __RDE('contenido_responder_tema_div_alert');
      }
    }
  }
}
