__('iniciar_editar_tema').addEventListener('click',editar_tema,false);
function editar_tema() {
  var tema, contenido, valor, error_campo;
  tema = __('tema_editar_tema').value;
  contenido = __('contenido_editar_tema').value;
  error_campo = "Por favor rellene este campo";
  if(tema !="" && tema.length >= 9 &&
    contenido != "" && contenido.length >=270){
    __('form_editar_tema').submit();
  }else{
    if(tema == ""){
      __ADI('tema_editar_tema');
      __ADE('tema_editar_tema_div_alert',error_campo);
    }else{
      if(tema.length < 9){
        __RDI('tema_editar_tema');
        __RDE('tema_editar_tema_div_alert');
        __ADI('tema_editar_tema');
        __ADE('tema_editar_tema_div_alert','El titulo debe tener minimo 9 caracteres');
      } else{
        __RDI('tema_editar_tema');
        __RDE('tema_editar_tema_div_alert');
      }
    }
    if (contenido == "") {
      __ADI('contenido_editar_tema');
      __ADE('contenido_editar_tema_div_alert',error_campo);
    } else {
      if(contenido.length < 270){
        __RDI('contenido_editar_tema');
        __RDE('contenido_editar_tema_div_alert');
        __ADI('contenido_editar_tema');
        __ADE('contenido_editar_tema_div_alert','El contenido debe de tener minimo 270 caracteres');
      } else {
        __RDI('contenido_editar_tema');
        __RDE('contenido_editar_tema_div_alert');
      }
    }
  }
}
